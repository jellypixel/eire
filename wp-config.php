<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'dbw_eire' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'ceF|W>,*j/*)ME.YDZ+pG~iCU|zy*,!JJ3V>Evd]TOCl[>e+p.!b#3~lAi8&gH>^' );
define( 'SECURE_AUTH_KEY',  'A*O#,?0$%Tu`izoPk]=&:_l~cwH_ZPG]Rl.4y1Vq8!q Y3A>%70Q.]_5Zn12dnB.' );
define( 'LOGGED_IN_KEY',    'm2D)RH2?i9ikiz7_s=$c7V@F6AkI]6xwrJRTu]=^8kVYly8|h_0kT7h1w57J~p67' );
define( 'NONCE_KEY',        '#:c/-.L{f1ySq<U#9p1=M{3xmx/o3!zeFl3SCOomjd,*#][zB1e,~Dxn+>l.RaeA' );
define( 'AUTH_SALT',        'A6<8<,8jqR:>uQ^FddO]7a%E 60CTYVYl)d7/]P5j$_>hFN`_&M%_`#:Y3*M:Iu3' );
define( 'SECURE_AUTH_SALT', 'tF?dw$^N_/B0tk(_}:xEAZ[c]nZ-%J2}w+1t}c{H}DJM.RW*/bJwB97KB%N|->Vi' );
define( 'LOGGED_IN_SALT',   'Qf[W$&%PJR$%C!S!)c2 EXn #-3{!lboxE!657Egv0XOn#%`EqnR~t:d:B|?,i3`' );
define( 'NONCE_SALT',       'f8}w}fv2E9{Fcg p6PDH*-x|^MRx?~X;L|c?ZSww%d_bpVL&$KuWDMRq+0Ed[V{X' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'dbw_eir_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

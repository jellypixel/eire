<?php return array(
    'root' => array(
        'pretty_version' => '4.2.0',
        'version' => '4.2.0.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'b886b64a7388ec03ddfb767fbd07d14b9f6fd580',
        'name' => 'a-z-listing/a-z-listing',
        'dev' => false,
    ),
    'versions' => array(
        'a-z-listing/a-z-listing' => array(
            'pretty_version' => '4.2.0',
            'version' => '4.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'b886b64a7388ec03ddfb767fbd07d14b9f6fd580',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.19.0',
            'version' => '1.19.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'reference' => 'b5f7b932ee6fa802fc792eabd77c4c88084517ce',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php54' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'symfony/polyfill-php55' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'symfony/polyfill-php56' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'symfony/polyfill-php70' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
    ),
);

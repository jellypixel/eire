<?php
	$contact_website = get_the_author_meta('user_url' );
	$contact_facebook = get_the_author_meta('facebook' );
	$contact_twitter = get_the_author_meta('twitter' );
	$contact_googleplus = get_the_author_meta('googleplus' );
	$contact_linkedin = get_the_author_meta('linkedin' );

	$author_meta = get_the_author_meta( 'description' );
	$author_meta_id = get_the_author_meta( 'ID' );
	$author_post_url = get_author_posts_url( $author_meta_id );
	
	if ( !empty( $author_meta ) )
	{
		?>
            <div class="single-author-wrapper">
                <a href="<?php echo esc_url($author_post_url); ?>">
                    <div class="single-author-image">
                        <?php
                            echo get_avatar( get_the_author_meta( 'user_email' ), 150, '', get_the_author_meta( 'display_name' ) );
                        ?>
                    </div>
                </a>
                <div class="single-author-content">
                	<div class="single-author-title">
                    	About Author
                    </div>
                    <div class="single-author-name">        	
                        <h4>
                            <a href="<?php echo esc_url($author_post_url); ?>">                    
                                <?php the_author(); ?>                    
                            </a>
                        </h4>
                    </div>
                    <div class="single-author-description">
                        <?php	
                            echo esc_html(get_the_author_meta( 'description' ));
                        ?>
                    </div>
                    <div class="single-author-contact">
                        <?php
                            if ( !empty( $contact_website ) ) 
                            {
                                ?>
                                    <a href="<?php echo esc_url($contact_website); ?>" title="<?php esc_html_e("Website", "seedlet"); ?>">
                                        <div class="glyph-home-alt"></div>
                                    </a>        
                                <?php					
                            }
                            if ( !empty( $contact_facebook ) ) 
                            {
                                ?>
                                    <a href="<?php echo esc_url($contact_facebook); ?>" title="<?php esc_html_e("Facebook", "seedlet"); ?>">
                                        <div class="glyph-facebook"></div>
                                    </a>        
                                <?php					
                            }
                            if ( !empty( $contact_twitter ) ) 
                            {
                                ?>
                                    <a href="<?php echo esc_url($contact_twitter); ?>" title="<?php esc_html_e("Twitter", "seedlet"); ?>">
                                        <div class="glyph-twitterbird"></div>
                                    </a>        
                                <?php					
                            }
                            if ( !empty( $contact_googleplus ) ) 
                            {
                                ?>
                                    <a href="<?php echo esc_url($contact_googleplus); ?>" title="<?php esc_html_e("Google Plus", "seedlet"); ?>">
                                        <div class="glyph-googleplus"></div>
                                    </a>        
                                <?php					
                            }
                            if ( !empty( $contact_linkedin ) ) 
                            {
                                ?>
                                    <a href="<?php echo esc_url($contact_linkedin); ?>" title="<?php esc_html_e("LinkedIn", "seedlet"); ?>">
                                        <div class="glyph-linkedin"></div>
                                    </a>        
                                <?php					
                            }
                        ?>
                    </div>
                </div>
            </div>
        <?php
	}
?>
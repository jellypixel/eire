<nav class="row nav-post">            	
    <div id="nav-previous-post" class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <?php
			previous_post_link( '%link', '&#8592; ' . esc_html__('Previous Post', 'seedlet') );
        ?>
    </div>                                                            
    <div id="nav-next-post" class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <?php
			next_post_link( '%link', esc_html__('Next Post', 'seedlet') . ' &#8594' );
        ?>
    </div>
</nav>
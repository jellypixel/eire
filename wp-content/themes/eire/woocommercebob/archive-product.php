<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $wp_query;
$term =	$wp_query->queried_object;

get_header( 'shop' );
?>

<div class="woocommerce-archive-wrapper">
	<div class="container">
    	
		<?php
            /**
             * Hook: woocommerce_before_main_content.
             *
             * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
             * @hooked woocommerce_breadcrumb - 20
             * @hooked WC_Structured_Data::generate_website_data() - 30
             */
            do_action( 'woocommerce_before_main_content' );							
        ?>
     
    
    	<header class="woocommerce-products-header">
        	<?php				
				$vendor_data = get_term_meta( $term->term_id, 'vendor_data', true );
				
				if ( $vendor_data )
				{
					$vendor_avatar = wp_get_attachment_image_url( absint( $vendor_data['logo'] ), 'full' );
				}
			?>        		
            
            <div class="vendor-archive-header">
            	<div class="vendor-archive-header-top">
                	<?php
						if ( ! empty( $vendor_data['logo'] ) && 'yes' === get_option( 'wcpv_vendor_settings_vendor_display_logo', 'yes' ) ) {
							?>
                                <div class="vendor-archive-avatar" style="background-image:url(<?php echo $vendor_avatar; ?>);">
                                </div>	
                    		<?php
                    	}
					?>
                    <div class="vendor-archive-header-top-content">
                        <div class="vendor-archive-name">
                        	<?php 
								if ( apply_filters( 'woocommerce_show_page_title', true ) ) 
								{
									?>
                                		<h1><?php woocommerce_page_title(); ?></h1>
                                	<?php
								}
							?>
                        </div>
                        <?php
							if ( !is_shop() && !is_product_category() )
							{
								$show_ratings = get_option( 'wcpv_vendor_settings_vendor_review', 'yes' );
								
								if ( 'yes' === $show_ratings ) {
									?>
										<div class="vendor-archive-rating">
											<?php echo WC_Product_Vendors_Utils::get_vendor_rating_html( $term->term_id ); ?>
										</div>
									<?php	
								}
							}
						?>                        
                    </div>
	            </div>
                <?php
					if ( ! empty( $vendor_data['profile'] ) && 'yes' === get_option( 'wcpv_vendor_settings_vendor_display_profile', 'yes' ) ) 
					{
						?>
                        	<div class="vendor-archive-header-bottom">
                                <div class="vendor-archive-description">
                                	<?php echo '<div class="wcpv-vendor-profile entry-summary">' . wpautop( wp_kses_post( do_shortcode( $vendor_data['profile'] ) ) ) . '</div>' . PHP_EOL; ?>
                                </div>
                            </div>
                        <?php
					}
				?>
            </div>
        
            <?php
				/**
				 * Hook: woocommerce_archive_description.
				 *
				 * @hooked woocommerce_taxonomy_archive_description - 10
				 * @hooked woocommerce_product_archive_description - 10
				 */
				 
				//remove_action( 'woocommerce_archive_description', array( $self, 'display_vendor_logo_profile' ) );
				
				remove_all_actions( 'woocommerce_archive_description' );
				add_action( 'woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10 );
				add_action( 'woocommerce_archive_description', 'woocommerce_product_archive_description', 10 );
				
				do_action( 'woocommerce_archive_description' );
            ?>
        </header>
        
        <div class="woocommerce-archive-content-wrapper">
        	<div class="woocommerce-sidebar-wrapper">
            	<div class="woocommerce-sidebar-inner">
                    <div class="woocommerce-sidebar-header">
                        Filters
                        <div class="filter-menu-close-wrapper mobile">
                            <div class="burgerbar top"></div>
                            <div class="burgerbar bottom"></div>  
                        </div>
                    </div>
                    <?php
                        /**
                         * Hook: woocommerce_sidebar.
                         *
                         * @hooked woocommerce_get_sidebar - 10
                         */
                        //do_action( 'woocommerce_sidebar' );
                        
                        dynamic_sidebar( 'shop ' );
                    ?>
                </div>
            </div>
            
            <div class="woocommerce-archive-content">        
				<?php
                    if ( woocommerce_product_loop() ) {
                    
                        /**
                         * Hook: woocommerce_before_shop_loop.
                         *
                         * @hooked woocommerce_output_all_notices - 10
                         * @hooked woocommerce_result_count - 20
                         * @hooked woocommerce_catalog_ordering - 30
                         */
						// Remove original count and sort
						remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
						remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
						
						// Get new count and sort
						add_action( 'woocommerce_before_shop_loop', 'get_archive_content_header', 20);
						
						function get_archive_content_header() 
						{							
							?>
								<div class="woocommerce-archive-content-header">
                                	<div class="woocommerce-archive-content-header-left">
                                    	All Items
                                    </div>
                                    <div class="woocommerce-archive-content-header-right">							
										<?php echo woocommerce_catalog_ordering(); ?>
                                        <div class="filter-toggler">
                                        	<svg height="24" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><polygon points="22 3 2 3 10 12.46 10 19 14 21 14 12.46 22 3"/></svg>
                                        </div>
                                    </div>
                            	</div>
                            <?php
						}		
						
                        do_action( 'woocommerce_before_shop_loop' );
                    
                        woocommerce_product_loop_start();
                    
                        if ( wc_get_loop_prop( 'total' ) ) {
                            while ( have_posts() ) {
                                the_post();
                    
                                /**
                                 * Hook: woocommerce_shop_loop.
                                 */
                                do_action( 'woocommerce_shop_loop' );
                    
                                wc_get_template_part( 'content', 'product' );
                            }
                        }
                    
                        woocommerce_product_loop_end();
                    
                        /**
                         * Hook: woocommerce_after_shop_loop.
                         *
                         * @hooked woocommerce_pagination - 10
                         */
                        do_action( 'woocommerce_after_shop_loop' );
                    } else {
                        /**
                         * Hook: woocommerce_no_products_found.
                         *
                         * @hooked wc_no_products_found - 10
                         */
                        do_action( 'woocommerce_no_products_found' );
                    }
                ?>
            </div>
        </div>
        
        <?php
			/**
			 * Hook: woocommerce_after_main_content.
			 *
			 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
			 */
			do_action( 'woocommerce_after_main_content' );
		?>
        
    </div>
</div>

<?php
	get_footer( 'shop' );
?>
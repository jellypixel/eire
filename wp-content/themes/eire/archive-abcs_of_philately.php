<?php
	get_header();	
?>

	<div id="content" class="site-content page-wrapper">
        <div class="entry-content">
			
			<div class="page-title-wrapper">				
				<h1>The ABCs of Philately</h1>
			</div>
			
			<div class="content-wrapper">
				<div class="content-left">
					<?php    				
						// Get content from last page using template 'template-abcs-of-philately.php'
							$args = array(
								'post_type' => 'page', 
								'meta_key' => '_wp_page_template', 
								'meta_value' => 'template-abcs-of-philately.php',
								'posts_per_page' => 1
							);
							
							$query = new WP_Query( $args );
							
							if ( $query->have_posts() ) 
							{
								while ( $query->have_posts() ) 
								{
									$query->the_post();
									
									//the_content();
								}
							}
							
							wp_reset_postdata();
						
						// Actual Archive Content
							echo do_shortcode('[a-z-listing display="posts" post-type="abcs_of_philately"]');
						
							/*if ( have_posts() )
							{
								while( have_posts() )
								{
									the_post();
									global $post;
	
									the_content();
								}
							}
	
							wp_reset_postdata();*/
					?>    
				</div>
									
				<div class="content-right">
                	<?php
						if ( is_active_sidebar( 'global-sidebar' ) || is_active_sidebar( 'abcs_of_philately-sidebar' ) )
						{
							?>
                            	<div class="sidebar-wrapper">                                	
                                    <?php
										dynamic_sidebar( 'global-sidebar' );
										
										dynamic_sidebar( 'abcs_of_philately-sidebar' );
									?>                                
                                </div>
                            <?php
 
						}
					?>  
				</div>
			</div>
            
        </div>
    </div>
               
<?php
	get_footer();
?>
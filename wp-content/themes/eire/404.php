<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Seedlet
 * @since 1.0.0
 */

get_header();
?>

	<div class="entry-content">
    	<div class="error-404 not-found">            
            <h3 class="page-title">                
                <?php _e( 'Oops! That page can&rsquo;t be found.', 'seedlet' ); ?>                
            </h3>
            <a href="<?php echo site_url(); ?>" title="Go back to home">
                <div class="std-button">
                    Back to Homepage
                </div>
            </a>       
        </div>
    </div>

<?php
get_footer();

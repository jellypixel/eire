<?php
	get_header();	
?>

	<div id="content" class="site-content page-wrapper">
        <div class="entry-content">
			
			<div class="page-title-wrapper">				
				<h1><?php the_title(); ?></h1>
			</div>
			
			<div class="content-wrapper">
				<div class="content-left">
					<?php    					
						if ( have_posts() )
						{
							while( have_posts() )
							{
								the_post();
								global $post;

								the_content();
							}
						}

						wp_reset_postdata();
					?>    
				</div>
								
				<div class="content-right">
					<div class="sidebar-wrapper">
						<?php dynamic_sidebar( 'single-sidebar' ); ?>
					</div>
				</div>
			</div>
        </div>
    </div>
               
<?php
	get_footer();
?>
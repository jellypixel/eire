var $j = jQuery.noConflict();

var vpHeight = $j( window ).height();
var headerHeight = 0;
var headerEl;
var flexslider = { vars:{} }; 

$j(function(){	
	// Mobile Menu
		var nav = $j( '.menu-wrapper' );
		var headerWrapper = $j( '.header-wrapper' );
		var slidingOverlay = $j( '.slidingmenu-overlay' );
	
		// Burger bar on click
		$j( '.nav-toggler' ).click( function() {
			// Sliding Menu
			if ( nav.hasClass( 'slidingmenu' ) )
			{
				// Close Sliding Menu
				if ( headerWrapper.hasClass( 'open' ) )
				{
					nav.animate({ 'left' : '-220px' }, function() {
						nav.hide();	
						slidingOverlay.fadeOut(400);
					});
				}
				// Open Sliding Menu
				else
				{
					nav.show();
					nav.animate({ 'left' : '0' }, 400);
					slidingOverlay.fadeIn(400);
				}
			}
			// Dropdown Menu
			else
			{	
				nav.slideToggle( 400 );
			}
			
			headerWrapper.toggleClass( 'open' );
		});
		
		// Close Sliding Menu on Overlay Click
		slidingOverlay.click( function() {
			nav.animate({ 'left' : '-220px' }, function() {
				nav.hide();	
				slidingOverlay.fadeOut(400);
				
				headerWrapper.toggleClass( 'open' );
			});
		});
		
	// Sticky
		headerEl = $j( 'header' );
		if( headerEl.length && headerEl.hasClass( 'stickyheader' ) ) 
		{
			var topBar = $j( '.topbar-wrapper' ).outerHeight();			
			var headerBar = $j( '.header-wrapper' ).outerHeight();
			topBar = topBar ? topBar : 0;
			headerBar = headerBar ? headerBar : 0;
			headerHeight = topBar + headerBar;
			
			if( $j( '.stickyheader:not(.transparentheader)' ).length ) {			
				$j( 'body' ).css({ 'padding-top' : headerHeight });
			}
		}
		
	// Search
		$j( 'body' ).on( 'click', '.search-wrapper svg', function() {
			headerEl.toggleClass( 'searchopen' );
			
			$j( '.search-input' ).focus();
		});
		
	// Gutenberg Block Image - Fit Container
		$j( '.is-style-fit-container' ).parent( '.wp-block-column' ).css({ 'position' : 'relative' });
	
	// To Top	
		$j( '#toTop' ).click( function() {
			$j( 'html, body' ).animate({
				scrollTop: 0
			}, 400);
		});
		
	// CPT - Single - ABCs of Philately
		$j('#collection-image-thumbnail-slider').flexslider({
   			animation: "slide",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			itemWidth: 210,
			itemMargin: 5,
			asNavFor: '#collection-image-slider',
			minItems: getGridSize(),
	        maxItems: getGridSize()
		});
		 
		$j('#collection-image-slider').flexslider({
			animation: "slide",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			sync: "#collection-image-thumbnail-slider",
			prevText:'',
			nextText:''	
		});
		
		
		
});

function getGridSize() {
	return (window.innerWidth < 768) ? 2 :
		   (window.innerWidth < 992) ? 3 : 4;
}
 

$j( window ).resize( function() {
	vpHeight = $j( window ).height();
	
	// Flexslider - Carousel Dynamic Childrens
	var gridSize = getGridSize();
 
    flexslider.vars.minItems = gridSize;
    flexslider.vars.maxItems = gridSize;
});

$j( window ).scroll( function() {
	if ( $j( window ).scrollTop() > vpHeight ) {
		$j( '#toTop' ).fadeIn(400);
	}
	else {
		$j( '#toTop' ).fadeOut(400);	
	}
	
	if( $j( '.stickyheader' ).length ) 
	{
		if ( $j( window ).scrollTop() > headerHeight ) {
			headerEl.addClass( 'onScroll' );
		}
		else {
			headerEl.removeClass( 'onScroll' );
		}
	}
});
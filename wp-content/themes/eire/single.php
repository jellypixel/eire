<?php 
	get_header();  
?>

<?php
	if ( is_singular( 'post' ) )
	{
		get_template_part( 'single', 'post' );	
	}
	else if ( is_singular( 'abcs_of_philately' ) )
	{
		get_template_part( 'single', 'abcs-of-philately' );	
	}
?>    

<?php 
	get_footer(); 
?>

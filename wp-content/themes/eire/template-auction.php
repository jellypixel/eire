<?php
/*
	Template Name: Archive: Auction
*/
?>

<?php
	get_header();	
	
	/*  $args = array(
       'public'   => true,
       '_builtin' => false,
    );

    $output = 'names'; // names or objects, note names is the default
    $operator = 'and'; // 'and' or 'or'

    $post_types = get_post_types( $args, $output, $operator ); 

    foreach ( $post_types  as $post_type ) {

       echo '<p>' . $post_type . '</p>';
    }*/
?>

	<div id="content" class="site-content page-wrapper">
        <div class="entry-content">
			
			<div class="page-title-wrapper">				
				<h1><?php the_title(); ?></h1>
			</div>
			
			<div class="content-wrapper">
				<div class="content-left">
					<?php    					
						if ( have_posts() )
						{
							while( have_posts() )
							{
								the_post();
								global $post;

								the_content();
							}
						}

						wp_reset_postdata();
					?>    
				</div>
								
				<div class="content-right">
                	<div class="sidebar-wrapper">
                    	<?php
							dynamic_sidebar( 'auction-sidebar' ); 
						?>
                    </div>
				</div>
			</div>
            
        </div>
    </div>
               
<?php
	get_footer();
?>
<?php
	/*	 	  
	 * Header	 
	 */
	 
	/* 
	 * Table of contents:
	 * 1. WORDPRESS HOOK: wp_head
	 * 2. WORDPRESS HOOK: wp_body_open
	 * 3. SKip Link
	 * 4. Logo
	 * 5. Menu Walker
	 */
?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>      
    <a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'seedlet' ); ?></a>

	<?php
		$headerClass = '';
		$transparent_menu = get_post_meta( $post->ID, '_transparent_menu_meta_key', true );
		
		if ( $transparent_menu == 'yes' )
		{
			$headerClass .= 'transparentheader';
		}
		
		$sticky_header = get_theme_mod( 'sticky_header', 'no' ); 
		if ( $sticky_header == 'yes' ) 		
		{
			$headerClass .= ' stickyheader';
		}
	?>

    <header class="<?php echo $headerClass; ?>">
    	<?php
			if ( is_active_sidebar( 'top-bar-left' ) || is_active_sidebar( 'top-bar-right' ) ) 
			{
				?>
                	<div class="topbar-wrapper entry-container">
                    	<div class="container">                    
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">                            	
                                    <?php
                                        if ( is_active_sidebar( 'top-bar-left' ) )
                                        {
                                            ?>
                                                <div class="slimbar-left">
                                                    <?php dynamic_sidebar( 'top-bar-left' ); ?>
                                                </div>
                                            <?php
                                        }
                                    ?>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                    <?php
                                        if ( is_active_sidebar( 'top-bar-right' ) )
                                        {
                                            ?>
                                                <div class="slimbar-right">
                                                    <?php dynamic_sidebar( 'top-bar-right' ); ?>
                                                </div>
                                            <?php
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
      				</div>
                <?php				
			}
		?>
    	
        <div class="header-wrapper entry-container">
            <div class="container">        
                <div class="logo-wrapper">
                    <?php
                        $display_title_and_tagline = get_theme_mod( 'display_title_and_tagline', true );
						$alternate_logo = get_theme_mod( 'alternate_logo' );
                        $logoEl = '';
                        
                        if ( has_custom_logo() && $display_title_and_tagline )
                        {	
							if ( $transparent_menu == 'yes' && !empty( $alternate_logo ) && substr( $alternate_logo, -14 ) != 'selectlogo.png' )
							{
								?>
                                	<a href="<?php echo site_url(); ?>" rel="home">
	                                	<img src="<?php echo get_theme_mod( 'alternate_logo' ) ?>" class="custom-logo" alt="<?php echo get_bloginfo( 'name' ); ?>" >
                                    </a>
                                <?php
							}
							else
							{
								$logoEl = get_custom_logo();
								echo $logoEl;
							}
                        }	
                    ?>
                    
                    <div class="nav-toggler burger-wrapper">
                        <div class="burgerbar top"></div>
                        <div class="burgerbar middle"></div>
                        <div class="burgerbar bottom"></div>                    
                    </div>
                </div>            
                
                <?php
                    if ( has_nav_menu( 'mainmenu-location' ) )
                    {
						wp_nav_menu(
							array(
								'theme_location'  => 'mainmenu-location',
								'menu_class'      => 'mainmenu',
								'container_class' => 'menu-wrapper primary-navigation',
								'items_wrap'      => '<ul id="%1$s" class="%2$s">
													  	  <li class="slidingmenu-logo-wrapper">
														  </li>
														   %3$s
													  </ul>'
							)
						);   
                    }						
                ?>      
                
                <?php
					$search_popup = get_theme_mod( 'search_popup', 'yes' );
					
					if ( $search_popup == 'yes' ) 
					{
						?>
                        	<div class="search-wrapper">
                            	<form id="searchform">
                                    <input id="s" name="s" class="search-input" type="text" placeholder="Search...">
                                    <input type="submit">
                                </form>
								<svg width="100%" height="100%" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;">
                                    <g transform="matrix(1,0,0,1,0,-120)">
                                        <g id="Search" transform="matrix(0.0629083,0,0,0.0616947,-46.7526,100.688)">
                                            <rect x="743.187" y="313.024" width="413.3" height="421.43" style="fill:none;"/>
                                            <clipPath id="_clip1">
                                                <rect x="743.187" y="313.024" width="413.3" height="421.43"/>
                                            </clipPath>
                                            <g clip-path="url(#_clip1)">
                                                <g transform="matrix(15.8553,0,0,16.1672,711.476,-1659.37)">
                                                    <path d="M20.038,141.452L26.36,147.774C26.75,148.165 27.384,148.165 27.774,147.774C28.165,147.384 28.165,146.75 27.774,146.36L21.452,140.038C23.043,138.129 24,135.676 24,133C24,126.929 19.071,122 13,122C6.929,122 2,126.929 2,133C2,139.071 6.929,144 13,144C15.676,144 18.129,143.043 20.038,141.452ZM19.374,139.351C20.997,137.723 22,135.478 22,133C22,128.033 17.967,124 13,124C8.033,124 4,128.033 4,133C4,137.967 8.033,142 13,142C15.478,142 17.723,140.997 19.351,139.374C19.355,139.37 19.359,139.366 19.363,139.363C19.366,139.359 19.37,139.355 19.374,139.351Z"/>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                        <?php	
					}
				?>    
                
                <div class="slidingmenu-overlay">
                </div>            
            </div>
        </div>
    </header>


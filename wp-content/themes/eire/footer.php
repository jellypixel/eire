<?php
	/*	 	  
	 * Footer	 
	 */
	 
	/* 
	 * Table of contents:
	 * 1. Logo
	 * 2. WORDPRESS HOOK: wp_footer
	 */
?>

	<footer role="contentinfo" aria-label="<?php esc_attr_e( 'Footer', 'seedlet' ); ?>">
        <?php
			if ( is_active_sidebar( 'bottom-bar-left' ) || is_active_sidebar( 'bottom-bar-right' ) ) 
			{
				?>
					<div class="bottombar-wrapper entry-container">
                    	<div class="container">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">                            	
                                    <?php
                                        if ( is_active_sidebar( 'bottom-bar-left' ) )
                                        {
                                            ?>
                                                <div class="slimbar-left">
                                                    <?php dynamic_sidebar( 'bottom-bar-left' ); ?>
                                                </div>
                                            <?php
                                        }
                                    ?>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                    <?php
                                        if ( is_active_sidebar( 'bottom-bar-right' ) )
                                        {
                                            ?>
                                                <div class="slimbar-right">
                                                    <?php dynamic_sidebar( 'bottom-bar-right' ); ?>
                                                </div>
                                            <?php
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
				<?php				
			}
		?>
        		
	</footer>
    
    <div id="toTop">
    </div>

<?php wp_footer(); ?>

</body>
</html>

<?php
	get_header();	
?>

	<div id="content" class="site-content page-wrapper">
        <div class="entry-content">
			
			<div class="page-title-wrapper">								
                <div class="breadcrumb-wrapper">
                	<?php
						$cpt = get_post_type_object( 'abcs_of_philately' );
						$cpt_title = $cpt->labels->singular_name;
						$cpt_slug = $cpt->name;
					?>
                    Return to the 
                	<a 
                    	href="<?php echo get_post_type_archive_link( $cpt_slug ); ?>"
                        title="<?php echo $cpt_title; ?>"
					>
                    	<?php echo $cpt_title; ?>
                    </a>
                </div>
                <h1><?php the_title(); ?></h1>
			</div>
			
			<div class="content-wrapper">
				<div class="content-left">
					<?php  
						// Collection Image  				
							get_template_part( 'template-parts/collection-image' );						
							
						// Post Content
							get_template_part( 'template-parts/post-content' );							
							
						// Related Content
							get_template_part( 'template-parts/related-content' );			
							
						// Related Files
							get_template_part( 'template-parts/related-files' );	
						
						// Related Outside Links
							get_template_part( 'template-parts/related-outside-links' );
					?>    
				</div>
									
				<div class="content-right">
                	<?php
						if ( is_active_sidebar( 'global-sidebar' ) || is_active_sidebar( 'abcs_of_philately-sidebar' ) )
						{
							?>
                            	<div class="sidebar-wrapper">                                	
                                    <?php
										dynamic_sidebar( 'global-sidebar' );
										
										dynamic_sidebar( 'abcs_of_philately-sidebar' );
									?>                                
                                </div>
                            <?php
 
						}
					?>                    
				</div>
			</div>
            
        </div>
    </div>
               
<?php
	get_footer();
?>
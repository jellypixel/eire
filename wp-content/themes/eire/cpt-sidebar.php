<div class="sidebar-wrapper">
	<?php 
		$cpts =  array(
					'abcs_of_philately',
					'auction',					
					'auction_item',					
					'award_recipient',					
					'award_type',					
					'chapter',					
					'event',					
					'exhibit',					
					'exhibit_frame',					
					'featured_stamp',					
					'ipn',					
					'library_item',					
					'link',					
					'microcopy',					
					'newsletter',					
					'palmares',					
					'palmares_award',					
					'publication',					
					'video_tutorial',					
					'views_page_display',
				);
				
		foreach( $cpts as $cpt )
		{
			if ( is_post_type_archive( $cpt ) ) {		
				dynamic_sidebar( $cpt . '-sidebar' ); 
				echo  $cpt . '-sidebar' ;
				break;
			}
			else if ( is_singular( $cpt ) ) {
				dynamic_sidebar( 'single-' . $cpt . '-sidebar' ); 
				echo 'single-' . $cpt . '-sidebar' ;
				break;
			}
		}
	?>
</div>	
<?php
	if ( have_posts() )
	{
		?>
			<div class="post-content-wrapper section">
				<?php								
					while( have_posts() )
					{
						the_post();
						global $post;

						the_content();
					}
				?>
			</div>
		<?php
	}

	wp_reset_postdata();
?>
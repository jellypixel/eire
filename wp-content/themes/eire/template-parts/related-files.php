<?php
	$related_files = get_field( 'related_files' );
	if ( !empty( $related_files ) )
	{
		?>
			<div class="related-files-wrapper section">
				<div class="section-title">
					<h3>Related Files</h3>
				</div>
				<div class="section-content">
					<a href="<?php echo $related_files['url']; ?>"><?php echo $related_files['filename']; ?></a>
				</div>
			</div>
		<?php
	}
?>
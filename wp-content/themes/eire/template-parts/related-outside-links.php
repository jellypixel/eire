<?php
	if ( have_rows( 'collection-related_outside_links' ) )
	{
		?>
			<div class="collection-related-outside-links-wrapper section">
				<div class="section-title">
					<h3>Related Outside Links</h3>
				</div>
				<div class="section-content">		                                	
					<?php
						while( have_rows( 'collection-related_outside_links' ) )
						{
							the_row();
							
							$related_link = get_sub_field('related_outside_links');
							
							$related_link_url = $related_link['url'];
							$related_link_title = $related_link['title'];
							$related_link_target = $related_link['target'] ? $related_link['target'] : '_self';													
							?>
								<a 
									href="<?php echo $related_link_url; ?>" 
									title="<?php echo $related_link_title; ?>" 
									target="<?php echo $related_link_target; ?>"
								>
									<?php echo $related_link_title; ?>
								</a>                                                        
							<?php
						}
					?>
				</div>
			</div>
		<?php
	}
?>
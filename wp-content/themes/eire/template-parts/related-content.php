<?php
	if ( have_rows( 'collection-related_content' ) )
	{
		?>
			<div class="collection-related-content-wrapper section">
				<div class="section-title">
					<h3>Related Content</h3>
				</div>
				<div class="section-content">		                                	
					<?php
						while( have_rows( 'collection-related_content' ) )
						{
							the_row();
							
							$related_post = get_sub_field('related_content');
							
							$related_post_permalink = get_permalink( $related_post->ID );
							$related_post_title = get_the_title( $related_post->ID );
							?>
								<a href="<?php echo $related_post_permalink; ?>" title="<?php echo $related_post_title; ?>">
									<?php echo $related_post_title; ?>
								</a>                                                        
							<?php
						}
					?>
				</div>
			</div>
		<?php
	}	
?>
<?php
	if ( have_rows( 'collection-image' ) )
	{
		?>
			<div class="collection-image-wrapper section">
				<div id="collection-image-slider" class="flexslider nav-inside">
					<ul class="slides">
						<?php
							while( have_rows( 'collection-image' ) )
							{
								the_row();
								
								$image = get_sub_field('image');
								?>
									<li class="collection-image-slide" style="background-image:url(<?php echo $image['url']; ?>);">
									</li>
								<?php
							}
						?>
					</ul>							
				</div>
				<div id="collection-image-thumbnail-slider" class="flexslider">
					<ul class="slides">
						<?php
							while( have_rows( 'collection-image' ) )
							{
								the_row();
								
								$image = get_sub_field('image');
								?>
									<li class="collection-image-slide" style="background-image:url(<?php echo $image['url']; ?>);">
									</li>
								<?php
							}
						?>
					</ul>							
				</div>
			</div>
		<?php
	}
?>
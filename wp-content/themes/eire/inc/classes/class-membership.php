<?php

namespace ERE;
	
class Membership
{
	public static function init()
	{
		\add_shortcode('member-listings', array(__CLASS__, 'render_membership_listing') );
	}

	public static function render_membership_listing( $atts, $content = "")
	{
		$num_to_display  = 10;

		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$offset = ($paged == 1) ? 0 : ($paged-1) * $num_to_display;

		$members_query = new \WP_User_Query( array(
			'role'	=> 'Subscriber',
			'order' => 'ASC',
			'orderby'	=> 'display_name',
			'number' => $num_to_display, 
			'offset' => $offset,
			'meta_query'	=> array(
				'relation' => 'OR', 
				array(
					'key'	=> 'member_profile_status',
					'value' => 'inactive',
					'compare'	=> '!='
				),
				array(
					'key' => 'member_profile_status',
					'value'	=> '',
					'compare'	=> 'NOT EXISTS'
				)
			)
		) );

		$members = $members_query->get_results();

		ob_start(); ?>
		<table class='member-listings-tbl'>
			<thead>
				<th>Member Name</th>
				<th>EPA Member Number</th>
			</thead>
			<tbody>
				<?php
				if ( !empty($members) ) : foreach( $members as $member ) :
				$user_info = \get_userdata($member->ID);
				// $member_profile = \get_field('member_profile', 'user_' . $member->ID); | doesn't work for some reason
				$epa_member_number = \get_user_meta($member->ID, 'member_profile_mnr', true);
				?>
				<tr>
					<td>
						<a href='<?= get_author_posts_url($member->ID); ?>' target='_blank'>
							<?= $user_info->display_name; ?>
						</a>
					</td>
					<td><?= $epa_member_number; ?></td>
				</tr>
				<?php
				endforeach; endif; 
				?>
			<tbody>
		</table>

		<?php
		$total_user = $members_query->total_users;  
		$total_pages=ceil($total_user/$num_to_display);

		echo \paginate_links(array(  
			'base' => get_pagenum_link(1) . '%_%',  
			'format' => '?paged=%#%',  
			'current' => $paged,  
			'total' => $total_pages,  
			'prev_text' => 'Previous',  
			'next_text' => 'Next',
			'type'     => 'list',
		)); 
		
		\wp_reset_postdata(); 

		return ob_get_clean();
	}
}

Membership::init();
<?php 
if ( ! class_exists( 'Automattic_Custom_Parent' ) ) {

  class Automattic_Custom_Parent {
	  
	  public $fields  	   = array();
	  public $section 	   = '';
	  public $title   	   = '';
	  public $font_weights = array();
	  public $font_styles  = array();
	  public $google_fonts = array();
	  public $google_map   = array();
	  	  
	  public function __construct() {
		  
		/*$this->font_weights = array(
            '100' => __( 'Thin (Hairline)', 'seedlet' ),
            '200' => __( 'Extra Light (Ultra Light)', 'seedlet' ),
            '300' => __( 'Light', 'seedlet' ),
            '400' => __( 'Normal (Regular)', 'seedlet' ),
            '500' => __( 'Medium', 'seedlet' ),
            '600' => __( 'Semi Bold (Demi Bold)', 'seedlet' ),
            '700' => __( 'Bold', 'seedlet' ),
            '800' => __( 'Extra Bold (Ultra Bold)', 'seedlet' ),
            '900' => __( 'Black (Heavy)', 'seedlet' ),
        );
        
        $this->font_styles = array( 
	        'normal'  => __( 'Normal', 'seedlet' ),
	        'italic'  => __( 'Italic', 'seedlet' ),
	        'oblique' => __( 'Oblique', 'seedlet' ),
	        'initial' => __( 'Initial', 'seedlet' ),
	        'inherit' => __( 'Inherit', 'seedlet' ),
        );
        
        $this->google_fonts = array(
	    	'default'          => __( 'Theme Default', 'seedlet' ),
			'source_sans'      => 'Source Sans Pro',
			'open_sans'        => 'Open Sans',
			'oswald' 	       => 'Oswald',
			'playfair'         => 'Playfair Display',
			'montserrat'       => 'Montserrat',
			'raleway'          => 'Raleway',
			'droid' 	       => 'Droid Sans',
			'lato' 	           => 'Lato',
			'arvo' 		       => 'Arvo',
			'lora' 		       => 'Lora',
			'merriweather'     => 'Merriweather',
			'oxygen' 	       => 'Oxygen',
			'pt_serif'         => 'PT Serif',
			'pt_sans' 	       => 'PT Sans',
			'pt_sans_narrow'   => 'PT Sans Narrow',
			'cabin' 		   => 'Cabin',
			'fjalla' 		   => 'Fjalla One',
			'francois' 	       => 'Francois One',
			'josefin' 		   => 'Josefin Sans',
			'libre' 		   => 'Libre Baskerville',
			'arimo' 		   => 'Arimo',
			'ubuntu' 		   => 'Ubuntu',
			'bitter' 		   => 'Bitter',
			'droid' 		   => 'Droid Serif',
			'roboto' 		   => 'Roboto',
			'open_sans' 	   => 'Open Sans Condensed',
			'roboto_condensed' => 'Roboto Condensed',
			'roboto_slab' 	   => 'Roboto Slab',
			'yavone'           => 'Yanone Kaffeesatz',
			'rokkitt'          => 'Rokkitt',
		);
	    
	    $this->google_map = array(
		    'default' 	       => 'default',
			'source_sans'      => 'Source Sans Pro:400,700,400italic,700italic',
			'open_sans' 	   => 'Open Sans:400italic,700italic,400,700',
			'oswald' 		   => 'Oswald:400,700',
			'playfair' 		   => 'Playfair Display:400,700,400italic',
			'montserrat' 	   => 'Montserrat:400,700',
			'raleway' 		   => 'Raleway:400,700',
			'droid' 		   => 'Droid Sans:400,700',
			'lato' 			   => 'Lato:400,700,400italic,700italic',
			'arvo' 			   => 'Arvo:400,700,400italic,700italic',
			'lora' 			   => 'Lora:400,700,400italic,700italic',
			'merriweather'     => 'Merriweather:400,300italic,300,400italic,700,700italic',
			'oxygen'           => 'Oxygen:400,300,700',
			'pt_serif'         => 'PT Serif:400,700',
			'pt_sans'          => 'PT Sans:400,700,400italic,700italic',
			'pt_sans_narrow'   => 'PT Sans Narrow:400,700',
			'cabin' 		   => 'Cabin:400,700,400italic',
			'fjalla' 		   => 'Fjalla One:400',
			'francois' 		   => 'Francois One:400',
			'josefin' 		   => 'Josefin Sans:400,300,600,700',
			'libre' 		   => 'Libre Baskerville:400,400italic,700',
			'arimo' 		   => 'Arimo:400,700,400italic,700italic',
			'ubuntu' 		   => 'Ubuntu:400,700,400italic,700italic',
			'bitter' 		   => 'Bitter:400,700,400italic',
			'droid' 		   => 'Droid Serif:400,700,400italic,700italic',
			'roboto' 		   => 'Roboto:400,400italic,700,700italic',
			'open_sans' 	   => 'Open Sans Condensed:700,300italic,300',
			'roboto_condensed' => 'Roboto Condensed:400italic,700italic,400,700',
			'roboto_slab' 	   => 'Roboto Slab:400,700',
			'yavone' 		   => 'Yanone Kaffeesatz:400,700',
			'rokkitt' 		   => 'Rokkitt:400',
	    );*/
	  }
	  
	  public function add_control_base( $wp_customize ) {
		  
		  /**
	       * Create other options panel.
	       */
	      $wp_customize->add_section(
	        $this->section,
	        array(
	          'panel'      => 'automattic_panel',
	          'capability' => 'edit_theme_options',
	          'title'      => $this->title,
	        )
	      );
	
	      /**
	       * Create toggle between default and custom other.
	       */
	      $wp_customize->add_setting(
	        $this->section . '_active',
	        array(
	          'capability'        => 'edit_theme_options',
	          'sanitize_callback' => array( $this, 'sanitize_select' ),
	          'transport'         => 'refresh',
	          'default'           => 'default',
	        )
	      );
	
	      $wp_customize->add_control(
	        $this->section . '_active',
	        array(
	          'type'    => 'radio',
	          'section' => $this->section,
	          'label'   => $this->title,
	          'choices' => array(
	            'default' => __( 'Theme Default', 'seedlet' ),
	            'custom'  => __( 'Custom', 'seedlet' ),
	          ),
	        )
	      );
	  }
  
	  public function add_field( $field, $wp_customize ) {
		  
		$key		= isset( $field['key'] )     ? $field['key']     : '';
	    $default    = isset( $field['default'] ) ? $field['default'] : '';
	    $label	    = isset( $field['label'] )   ? $field['label']   : '';
	    $type	    = isset( $field['type'] )    ? $field['type']    : '';
	    $options	= isset( $field['options'] ) ? $field['options'] : array();
	    	       	       
	    switch ( $type ) {
		   
		   case 'text':
		   
			   $sanitize = isset( $field['sanitize_function'] ) ? $field['sanitize_function'] : 'sanitize_generic';
		   		   		
		   		$wp_customize->add_setting(
		          "codeable_$key",
		          array(
		            'default'           => esc_html( $default ),
		            'sanitize_callback' => array( $this, $sanitize ),
		            )
		        );     
		        
		        $wp_customize->add_control(
		          "codeable_$key",
		            array(
		              'label'             => $label,
		              'type'              => 'text',
		              'section'           => $this->section,
		              'active_callback' => function() use ( $wp_customize ) {
		                return ( 'custom' === $wp_customize->get_setting( $this->section . '_active' )->value() );
		              },
		            ) 
		        );
		        
		        break;
		        
		   case 'number':
		   		
		   		$sanitize = isset( $field['sanitize_function'] ) ? $field['sanitize_function'] : 'sanitize_numeric';
		   		$input_attrs = isset( $field['input_attrs'] ) ? $field['input_attrs'] : array( 'min'  => 0, 'max'  => 1000, 'step' => 0.1 );
		   		
		   		$wp_customize->add_setting(
		          "codeable_$key",
		          array(
		            'default'           => esc_html( $default ),
		            'sanitize_callback' => array( $this, $sanitize ),
		            )
		        );     
		        
		        $wp_customize->add_control(
		          "codeable_$key",
		            array(
		              'label'             => $label,
		              'type'              => 'number',
		              'section'           => $this->section,
		              'input_attrs'       => $input_attrs,
		              'active_callback' => function() use ( $wp_customize ) {
		                return ( 'custom' === $wp_customize->get_setting( $this->section . '_active' )->value() );
		              },
		            ) 
		        );
		        
		        break;
	       case 'color':
	       
		       $wp_customize->add_setting(
					"codeable_$key",
					array(
						'default' => esc_html( $default ),
					)
				);
				$wp_customize->add_control(
					new WP_Customize_Color_Control(
						$wp_customize,
						"codeable_$key",
						array(
							'section'         => $this->section,
							'label'           => $label,
							'active_callback' => function() use ( $wp_customize ) {
								return ( 'custom' === $wp_customize->get_setting( $this->section . '_active' )->value() );
							},
						)
					)
				);
				break;
			case 'select':
			
				$sanitize = isset( $field['sanitize_function'] ) ? $field['sanitize_function'] : 'sanitize_select';
							
				$wp_customize->add_setting(
					"codeable_$key",
					array(
						'default'           => $default,
						'sanitize_callback' => array( $this, $sanitize ),
		         )
			    );
			    		
		        $wp_customize->add_control(
		            "codeable_$key",
		            array(
		              'label'   		=> $label,
		              'type'    		=> 'select',
		              'section' 		=> $this->section,
		              'choices'         => $options,  
		              'active_callback' => function() use ( $wp_customize ) {
									return ( 'custom' === $wp_customize->get_setting( $this->section . '_active' )->value() );
					   } 
					)
				);
				break;
	        default:
	       		break;
	    }
	}
	
	public static function is_number( $var ) {
	    if ( $var == (string) (float) $var ) {
	        return (bool) is_numeric($var);
	    }
	    if ( $var >= 0 && is_string( $var ) && !is_float( $var ) ) {
	        return (bool) ctype_digit($var);
	    }
	    return (bool) is_numeric($var);
	}
	
	/**
     * Sanitize select.
     *
     * @param string $input The input from the setting.
     * @param object $setting The selected setting.
     *
     * @return string $input|$setting->default The input from the setting or the default setting.
     */
    public static function sanitize_select( $input, $setting ) {
      $input   = sanitize_key( $input );
      $choices = $setting->manager->get_control( $setting->id )->choices;
      return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
    }
    
	public static function sanitize_numeric( $input, $setting ) {
      return ( is_numeric( $input ) ? $input : $setting->default );
    }
    
    public static function sanitize_generic( $input, $setting ) {
	    return sanitize_key( $input );
    }
    
    /**
       * Sanitize font size input.
       *
       * @param string $input The input from the setting.
       * @param object $setting The selected setting.
       *
       * @return string $input|$setting->default The input from the setting or the default setting.
       */
      public static function sanitize_font_size_input( $input, $setting ) {
	    	      
	    $allowed_units = array( 'px', 'pt', 'pc', 'em', 'rem', 'ex', 'vh', 'vw', 'ch', 'mm', 'cm', 'in' );
	    foreach ( $allowed_units as $unit ) {
		    
		    $compare = $input;
		    $compare = str_replace( $unit, '', $compare );
		    if ( $compare !== $input && self::is_number( $compare ) ) {
			    
			    if ( $compare > 9  ) {
				    return $input;
			    }
		    }
	    }
	    	    
	    return $setting->default;
      }

      /**
       * Sanitize line height input.
       *
       * @param string $input The input from the setting.
       * @param object $setting The selected setting.
       *
       * @return string $input|$setting->default The input from the setting or the default setting.
       */
      public static function sanitize_line_height( $input, $setting ) {
	      	    
	    $allowed_values = array( 'initial', 'normal', 'unset', 'inherit' );
	    if ( in_array( $input, $allowed_values ) ) {
		    
		    return $input;
	    } else if ( self::is_number( $input ) ) {
		    
		    return $input;
	    } else {
		    
		    $allowed_units = array( 'em', '%' );
		    
		    foreach ( $allowed_units as $unit ) {
			    
			    $compare = $input;
			    $compare = str_replace( $unit, '', $compare );
			    if ( $compare !== $input && self::is_number( $compare ) ) {
				    return $input;
			    }
		    }
	    }
	    	    
        return $setting->default;
      }
      
      /**
       * Sanitize css units.
       *
       * @param string $input The input from the setting.
       * @param object $setting The selected setting.
       *
       * @return string $input|$setting->default The input from the setting or the default setting.
       */
      public static function sanitize_css_units( $input, $setting ) {
	      	    
	    $allowed_units = array( 'px', 'pt', 'pc', 'em', 'rem', 'ex', 'vh', 'vw', 'ch', 'mm', 'cm', 'in' );
	    foreach ( $allowed_units as $unit ) {
		    
		    $compare = $input;
		    $compare = str_replace( $unit, '', $compare );
		    if ( $compare !== $input && self::is_number( $compare ) ) {
			    return $input;
		    }
	    }
	    
	    return $setting->default;
      }
  }
}
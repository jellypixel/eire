<?php 
/**
 * Seedlet Theme: Sticky Header Class
 *
 * Enable/disable the sticky header
 *
 * @package Seedlet
 * @since 1.0.0
 */

if ( ! class_exists( 'automattic_Sticky_Header' ) ) {

  class automattic_Sticky_Header extends automattic_Custom_Parent {

    public function __construct() {
      
      parent::__construct();
      
      $this->section = 'automattic_color_options';
      $this->title   = __( 'Colors', 'automattic-seedlet-theme' );
      
      $fields  = array( 
            array( 'key' => 'color_1', 'css' => '--global--color-primary-hover', 'default' => '#3c8067', 'label' => __( 'Primary Hover', 'automattic-seedlet-theme' ), 'type' => 'color' ),
            array( 'key' => 'color_2', 'css' => '--global--color-secondary-hover', 'default' => '#336d58', 'label' => __( 'Secondary Hover', 'automattic-seedlet-theme' ), 'type' => 'color' ),
            array( 'key' => 'color_3', 'css' => '--global--color-black', 'default' => '#000000', 'label' => __( 'Black Color', 'automattic-seedlet-theme' ), 'type' => 'color' ),
            array( 'key' => 'color_4', 'css' => '--global--color-white', 'default' => '#FFFFFF', 'label' => __( 'White Color', 'automattic-seedlet-theme' ), 'type' => 'color' ),
            array( 'key' => 'color_5', 'css' => '--global--color-background-dark', 'default' => '#DDDDDD', 'label' => __( 'Dark Background Color', 'automattic-seedlet-theme' ), 'type' => 'color' ),
            array( 'key' => 'color_6', 'css' => '--global--color-text-selection', 'default' => '#EBF2F0', 'label' => __( 'Text Selection Color', 'automattic-seedlet-theme' ), 'type' => 'color' ),
            array( 'key' => 'color_7', 'css' => '--global--color-alert-success', 'default' => '#9acd32', 'label' => __( 'Success Alert Color', 'automattic-seedlet-theme' ), 'type' => 'color' ),
            array( 'key' => 'color_8', 'css' => '--global--color-alert-info', 'default' => '#0000ff', 'label' => __( 'Info Alert Color', 'automattic-seedlet-theme' ), 'type' => 'color' ),
            array( 'key' => 'color_9', 'css' => '--global--color-alert-warning', 'default' => '#ffd700', 'label' => __( 'Warning Alert Color', 'automattic-seedlet-theme' ), 'type' => 'color' ),
            array( 'key' => 'color_10', 'css' => '--global--color-alert-error', 'default' => '#ff8c69', 'label' => __( 'Error Alert Color', 'automattic-seedlet-theme' ), 'type' => 'color' ),
      );
      
      $this->fields = apply_filters( 'automattic_seedlet_options_variables', $fields, $this->section );

      /**
       * Register Customizer actions
       */
      add_action( 'customize_register', array( $this, 'automattic_customize_custom_colors_register' ) );

      /**
       * Enqueue color variables for customizer & frontend.
       */
      add_action( 'wp_enqueue_scripts', array( $this, 'automattic_custom_color_variables' ) );

    }
  /**
     * Add Theme Options for the Customizer.
     *
     * @param WP_Customize_Manager $wp_customize Theme Customizer object.
     */
    public function automattic_customize_custom_colors_register( $wp_customize ) {
      
      /**
       * Generate control base
       */
      $this->add_control_base( $wp_customize );

      /**
       * Create option for all the other options
       */
       foreach ( $this->fields as $field ) {
	   	 $this->add_field( $field, $wp_customize );
       }
    }

    /**
     * Generate other variables.
     */
    private function automattic_generate_custom_color_variables( $context = null ) {

      if ( $context === 'editor' ) {
        $theme_css = ':root .editor-styles-wrapper {';
      } else {
        $theme_css = ':root {';
      }
      
      foreach ( $this->fields as $field ) {
	      
	      $key     = isset( $field['key'] )     ? $field['key']     : '';
	      $css     = isset( $field['css'] )     ? $field['css']     : '';
	      $after   = isset( $field['after'] )   ? $field['after']   : '';
	      $default = isset( $field['default'] ) ? $field['default'] : '';
	      if ( $key && $css && !empty( get_theme_mod( "automattic_$key" ) && get_theme_mod( "automattic_$key" ) != $default ) ) {
			  $value      = get_theme_mod( "automattic_$key" );
			  $theme_css .= $css . ':' . $value . $after . ';';
	      }
      }
      
      $theme_css .= '}';
      
      return $theme_css;
    }

    /**
     * Customizer & frontend additional custom other variables.
     */
    public function automattic_custom_color_variables() {
      wp_enqueue_style( 'automattic-custom-overrides', get_stylesheet_directory_uri() . '/inc/automattic/assets/css/automattic-custom-overrides.css', array(), wp_get_theme()->get( 'Version' ) );
      if ( 'default' !== get_theme_mod(  $this->section . '_active' ) ) {
        wp_add_inline_style( 'automattic-custom-overrides', $this->automattic_generate_custom_color_variables() );
      }
    }
  }
  
  new automattic_Sticky_Header();
}
<?php

/**
 * Seedlet Theme: Customizer
 *
 * @package Seedlet
 * @since 1.0.0
 */

if ( ! class_exists( 'automattic_Customize' ) ) {
	/**
	 * Customizer Settings.
	 *
	 * @since 1.0.0
	 */
	class automattic_Customize {

    public function __construct() {

      /**
       * Register with Customizer to add a global panel
       */
      add_action( 'customize_register', array( $this, 'register' ) );

      /**
       * Enqueue color variables for customizer & frontend.
       */
      add_action( 'wp_enqueue_scripts', array( $this, 'automattic_customizer_scripts' ) );

	}

    public function register( $wp_customize ) {

		/**
		 * Create automattic Panel.
		 */
	    $wp_customize->add_panel( 'automattic_panel', array(
	      'title' => __( 'Additional Settings' ),
	      'description' => "Customizer Options", // Include html tags such as <p>.
	      'priority' => 220, // Mixed with top-level-section hierarchy.
	    ) );
    }

    public function automattic_customizer_scripts() {
      wp_enqueue_script( 'automattic_customizer_scripts', get_stylesheet_directory_uri() . '/inc/assets/js/customizer_additions.js', array(), wp_get_theme()->get( 'Version' ), false );
    }
  }
   
  new automattic_Customize();
}


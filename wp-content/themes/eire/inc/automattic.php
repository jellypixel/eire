<?php

/**
 * Blocks
 */
//require get_stylesheet_directory() . '/inc/blocks/seedlet-blocks.php';   

 /**
 * automattic Customizer Options
 */
require get_stylesheet_directory() . '/inc/customizer.php';
require get_stylesheet_directory() . '/inc/classes/class-automattic-custom-parent.php';

/**
 * Sticky header options.
 */

require get_stylesheet_directory() . '/inc/classes/class-automattic-sticky-header.php';

/**
 * Additional Codeable color options.
 */

//require get_stylesheet_directory() . '/inc/classes/class-automattic-custom-colors.php';

/**
 * Additional Codeable fonts options.
 */

//require get_stylesheet_directory() . '/inc/classes/class-codeable-custom-fonts.php';

/**
 * Additional Codeable spacing options.
 */

//require get_stylesheet_directory() . '/inc/classes/class-codeable-custom-spacing.php';

/**
 * Additional Codeable other options.
 */

//require get_stylesheet_directory() . '/inc/classes/class-codeable-custom-other.php';

/**
 * Additional Codeable element options.
 */

//require get_stylesheet_directory() . '/inc/classes/class-codeable-custom-elements.php';

/**
 * Additional Codeable woocommerce options.
 */

//require get_stylesheet_directory() . '/inc/classes/class-codeable-custom-woocommerce.php';

/**
 * Additional Codeable blocks options.
 */

//require get_stylesheet_directory() . '/inc/classes/class-codeable-custom-blocks.php';

/**
 * Additional Codeable components options.
 */

////require get_stylesheet_directory() . '/inc/classes/class-codeable-custom-components.php';

/**
 * Add Codeable Import Export Module.
 */

////require get_stylesheet_directory() . '/inc/modules/import-export/class-main.php';

/**
 * Add Codeable Title Remover Module.
 */

////require get_stylesheet_directory() . '/inc/modules/title-remover/class-main.php';
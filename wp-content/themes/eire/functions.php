<?php
	/*	 	  
	 * WORDPRESS: Functions
	 * Extend Wordpress built in functions and Seedlet's functions.
	 */
	 
	/* 
	 * Table of contents:
	 * 1. Constants
	 * 2. Register Style/Script	 
	 * 3. Theme Setup	  
	 *    a. Dequeue Leaks
	 *	  b. Theme Support
	 *    c. Image Size
	 *    d. Sidebar
	 *	  e. Menu
	 * 4. Theme Customizer - General
	 * 5. Theme Customizer - Set Default Color Palette
	 * 6. Theme Customizer - Alternate Logo
	 * 7. Theme Customizer - Sticky Header
	 * 8. Theme Customizer - Social Media 
	 * 9. Extend Existing Gutenberg Blocks
	 * 10. Numbered Pagination
	 * 11. Metabox
	 * 12. Woocommerce	
	 * 13. Widget
	 */

	// Constants
		define( 'THEMEDIR', get_template_directory() . '/' );
		define( 'THEMEURI', get_stylesheet_directory_uri() . '/' );
		
	// Register Style/Script
		// Frontend
			//add_action( 'wp_enqueue_scripts', 'enqueue_list', 12 );			
			add_action( 'wp_enqueue_scripts', 'enqueue_list', 12 );
		
			function enqueue_list() {
				// Style						
					//wp_enqueue_style( 'font-lato', 'https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap' );
					//wp_enqueue_style( 'font-raleway', 'https://fonts.googleapis.com/css2?family=Raleway:wght@400;700&display=swap' );								    								
					wp_enqueue_style( 'flexslider-css', THEMEURI . 'css/flexslider.css', array( 'seedlet-style',  'seedlet-style-navigation' ) );
					wp_enqueue_style( 'global-css', THEMEURI . 'css/global.css', array( 'seedlet-style',  'seedlet-style-navigation' ) );
					wp_enqueue_style( 'default-css', get_stylesheet_uri(), array( 'seedlet-style', 'seedlet-style-navigation' ) );
					
				// Script			
					wp_enqueue_script( 'jquery' );
					wp_enqueue_script( 'flexslider-js', THEMEURI . 'js/jquery.flexslider.js',  array( 'jquery' ), null, true );		
					wp_enqueue_script( 'default-js', THEMEURI . 'js/default.js',  array( 'jquery' ), null, true );				
			}		
	
		// Backend
			add_action('admin_enqueue_scripts', 'admin_enqueue_list', 12);
			
			function admin_enqueue_list() {
				//wp_enqueue_style( 'font-lato', 'https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap' );
				//wp_enqueue_style( 'font-raleway', 'https://fonts.googleapis.com/css2?family=Raleway:wght@400;700&display=swap' );				
				wp_enqueue_style( 'global-css', THEMEURI . 'css/global.css', array( 'seedlet-style', 'editor-styles', 'seedlet-style-navigation' ) );
				wp_enqueue_style( 'admin-css', THEMEURI . 'css/admin.css' );
			}
			
	// Prevent Render Blocking
		add_filter( 'style_loader_tag', 'add_google_font_stylesheet_attributes', 10, 2 );
		
		function add_google_font_stylesheet_attributes( $html, $handle ) {
			if ( 'seedlet-fonts' === $handle ) 
			{
				return str_replace( "rel='stylesheet'", "rel='stylesheet' media='print' onload=\"this.media='all'\"", $html );
			}
			
			return $html;
		}
			
	// Theme Setup
		add_action( 'after_setup_theme', 'initial_setup', 11 );
	
		function initial_setup()
		{	
			// Dequeue Leaks
				/*if ( !is_admin() ) {
					add_action( 'wp_enqueue_scripts', 'dequeue_leaks');
					
					function dequeue_leaks() {
						wp_deregister_style('common');
					}				
				}*/
		
			// Theme Support
				if ( function_exists( 'add_theme_support' ) ) {		
					add_theme_support( 'post-thumbnails', array( 'post' ) );
					add_theme_support( 'editor-styles' );
					add_theme_support( 'align-wide' );	
					
					// Non square custom logo
					add_theme_support( 'custom-logo', array(
						 'flex-height' => true,
						 'flex-width' => true
					) );		
					
					// Editor styles
					add_editor_style( 'css/global.css' );
					add_editor_style( 'css/editor.css' );					
					
					// Editor - Color Palette
					$colors_theme_mod = get_theme_mod( 'custom_colors_active' );
					$primary          = ( ! empty( $colors_theme_mod ) && 'default' === $colors_theme_mod || empty( get_theme_mod( 'seedlet_--global--color-primary' ) ) ) ? '#008000' : get_theme_mod( 'seedlet_--global--color-primary' );
					$secondary        = ( ! empty( $colors_theme_mod ) && 'default' === $colors_theme_mod || empty( get_theme_mod( 'seedlet_--global--color-secondary' ) ) ) ? '#333333' : get_theme_mod( 'seedlet_--global--color-secondary' );	
					$tertiary         = ( ! empty( $colors_theme_mod ) && 'default' === $colors_theme_mod || empty( get_theme_mod( 'seedlet_--global--color-tertiary' ) ) ) ? '#FF7900' : get_theme_mod( 'seedlet_--global--color-tertiary' );	
					$white            = ( ! empty( $colors_theme_mod ) && 'default' === $colors_theme_mod || empty( get_theme_mod( 'seedlet_--global--color-white' ) ) ) ? '#FFFFFF' : get_theme_mod( 'seedlet_--global--white' );
					$foreground       = ( ! empty( $colors_theme_mod ) && 'default' === $colors_theme_mod || empty( get_theme_mod( 'seedlet_--global--color-foreground' ) ) ) ? '#606060' : get_theme_mod( 'seedlet_--global--color-foreground' );		
					$background       = ( ! empty( $colors_theme_mod ) && 'default' === $colors_theme_mod || empty( get_theme_mod( 'seedlet_--global--color-background' ) ) ) ? '#F5f5f5' : get_theme_mod( 'seedlet_--global--color-background' );
					$border       = ( ! empty( $colors_theme_mod ) && 'default' === $colors_theme_mod || empty( get_theme_mod( 'seedlet_--global--color-border' ) ) ) ? '#DDDDDD' : get_theme_mod( 'seedlet_--global--color-border' );
					
					add_theme_support( 'editor-color-palette', array(
						array(
							'name'  => __( 'Primary', 'seedlet' ),
							'slug'  => 'primary',
							'color' => $primary,
						),
						array(
							'name'  => __( 'Secondary', 'seedlet' ),
							'slug'  => 'secondary',
							'color' => $secondary,
						),
						array(
							'name'  => __( 'Tertiary', 'seedlet' ),
							'slug'  => 'tertiary',
							'color' => $tertiary,
						),
						array(
							'name'  => __( 'White Color', 'seedlet' ),
							'slug'  => 'white-color',
							'color' => $white,
						),
						array(
							'name'  => __( 'Foreground', 'seedlet' ),
							'slug'  => 'foreground',
							'color' => $foreground,
						),
						array(
							'name'  => __( 'Background', 'seedlet' ),
							'slug'  => 'background',
							'color' => $background,
						),
						array(
							'name'  => __( 'Border', 'seedlet' ),
							'slug'  => 'border',
							'color' => $border,
						)			
					) );
			
					// Editor - Font Size
					add_theme_support(
						'editor-font-sizes',
						array(
							array(
								'name'      => __( 'H6', 'seedlet' ),
								'shortName' => __( 'H6', 'seedlet' ),
								'size'      => 15,
								'slug'      => 'H6',
							),
							array(
								'name'      => __( 'H5 / Default ', 'seedlet' ),
								'shortName' => __( 'H5', 'seedlet' ),
								'size'      => 20,
								'slug'      => 'H5',
							),
							array(
								'name'      => __( 'H4 ', 'seedlet' ),
								'shortName' => __( 'Normal', 'seedlet' ),
								'size'      => 25,
								'slug'      => 'H4',
							),
							array(
								'name'      => __( 'H3 ', 'seedlet' ),
								'shortName' => __( 'H3', 'seedlet' ),
								'size'      => 30,
								'slug'      => 'H3',
							),
							array(
								'name'      => __( 'H2 ', 'seedlet' ),
								'shortName' => __( 'H2', 'seedlet' ),
								'size'      => 35,
								'slug'      => 'H2',
							),
							array(
								'name'      => __( 'H1 ', 'seedlet' ),
								'shortName' => __( 'H1', 'seedlet' ),
								'size'      => 55,
								'slug'      => 'H1',
							)
						)
					);
					
					// Editor - Image Size
					/*add_filter( 'image_size_names_choose', 'add_custom_image_sizes' );
					
					function add_custom_image_sizes( $sizes ) {
						return array_merge( $sizes, array(
							'fit-container' => __( 'Fit Container' )
						) );
					}					*/
				}
		
			// Image Size
				if ( function_exists( 'add_image_size' ) ) {
					add_image_size( 'full-thumb', 1320 );
					add_image_size( 'post-thumb'. 660 );
				}
				
			// Single
				register_sidebar( array(
					'name'          => 'Single Sidebar',
					'id'            => 'single-sidebar',
					'before_widget' => '<div class="single-widget">',
					'after_widget'  => '</div>',
					'before_title'  => '<h4>',
					'after_title'   => '</h4>',
				) );
				
			// Sidebar
				register_sidebar( array(
					'name'          => 'Bottom Bar Left',
					'id'            => 'bottom-bar-left',
					'before_widget' => '<div class="footer-area-widget">',
					'after_widget'  => '</div>',
					'before_title'  => '<h4>',
					'after_title'   => '</h4>',
				) );
				
				register_sidebar( array(
					'name'          => 'Bottom Bar Right',
					'id'            => 'bottom-bar-right',
					'before_widget' => '<div class="footer-area-widget">',
					'after_widget'  => '</div>',
					'before_title'  => '<h4>',
					'after_title'   => '</h4>',
				) );
				
				unregister_sidebar( 'sidebar-1' );
				
			// CPT Sidebar
				register_sidebar( array(
					'name'          => 'Global Sidebar',
					'id'            => 'global-sidebar',
					'before_widget' => '<div class="single-widget">',
					'after_widget'  => '</div>',
					'before_title'  => '<h3>',
					'after_title'   => '</h3>',
				) );
			
				$cpts =  array(
					'abcs_of_philately',
					'auction',					
					'auction_item',					
					'award_recipient',					
					'award_type',					
					'chapter',					
					'event',					
					'exhibit',					
					'exhibit_frame',					
					'featured_stamp',					
					'ipn',					
					'library_item',					
					'link',					
					'microcopy',					
					'newsletter',					
					'palmares',					
					'palmares_award',					
					'publication',					
					'video_tutorial',					
					'views_page_display',
				);
				
				foreach( $cpts as $cpt )
				{
					register_sidebar( array(
						'name' 			=> ucwords( str_replace( '_', ' ', $cpt ) ),
						'id'            => $cpt . '-sidebar',
						'before_widget' => '<div class="cpt-widget">', 'after_widget'  => '</div>',	'before_title'  => '<h3>', 'after_title'   => '</h3>',
					) );
					
					/*register_sidebar( array(
						'name' 			=> 'Single ' . ucwords( str_replace( '_', ' ', $cpt ) ),
						'id'            => 'single-' . $cpt . '-sidebar',
						'before_widget' => '<div class="single-cpt-widget">', 'after_widget'  => '</div>',	'before_title'  => '<h4>', 'after_title'   => '</h4>',
					) );*/
				}
				
			// Menu
				if ( function_exists( 'register_nav_menus' ) ) {
					register_nav_menu('mainmenu-location', 'Main Menu');
					
					unregister_nav_menu( 'primary' );
					unregister_nav_menu( 'footer' );
					unregister_nav_menu( 'social' );
				}			
		}
						
	// Theme Customizer - General
		//require get_stylesheet_directory() . '/inc/automattic.php';
		
	// Theme Customizer - Set Default Color Palette.
		add_filter( 'seedlet_colors', 'themecustomizer_change_seedlet_colors' );
		
		function themecustomizer_change_seedlet_colors( $seedlet_colors ) {
			$seedlet_colors = array(
				array( '--global--color-primary', '#008000', __( 'Primary Color', 'seedlet' ) ),
				array( '--global--color-secondary', '#333333', __( 'Secondary Color', 'seedlet' ) ),
				array( '--global--color-tertiary', '#FF7900', __( 'Tertiary Color', 'seedlet' ) ),
				array( '--global--color-white', '#FFFFFF', __( 'White', 'seedlet' ) ),				
				array( '--global--color-foreground', '#606060', __( 'Foreground Color', 'seedlet' ) ),				
				array( '--global--color-background', '#f5f5f5', __( 'Background Color', 'seedlet' ) ),
				array( '--gray', '#aaaaaa', __( 'Gray', 'seedlet' ) ),				
				array( '--global--color-border', '#dddddd', __( 'Borders Color', 'seedlet' ) )
			);
			
			return $seedlet_colors;
		}
		
	// Theme Customizer - Alternate Logo
		add_action( 'customize_register', 'themecustomizer_alternate_logo' );
		
		function themecustomizer_alternate_logo($wp_customize)
		{ 
			$wp_customize->add_setting( 'alternate_logo', array(
				'default' => get_theme_file_uri('img/selectlogo.png'), // Default
				'sanitize_callback' => 'esc_url_raw'
			));
		 
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'alternate_logo_control', array(
				'label' => 'Alternate Logo',
				'priority' => 9,
				'section' => 'title_tagline',
				'settings' => 'alternate_logo',
				'button_labels' => array(
									  'select' => 'Select logo',
									  'remove' => 'Remove',
									  'change' => 'Change logo',
								   )
			)));		 
		}
		
	// Theme Customizer - Sticky Header
		add_action( 'customize_register', 'themecustomizer_header' );
		
		function themecustomizer_header($wp_customize)
		{ 
			$wp_customize->add_section( 'header_section', array(
				'title' => __( 'Header' ),
				'description' => __( '' ),
				'panel' => '', 
				'priority' => 22,
				'capability' => 'edit_theme_options',
				'theme_supports' => '',
			) );
			
			function sanitize_select( $input, $setting ) {
				$input = sanitize_key( $input );
				$choices = $setting->manager->get_control( $setting->id )->choices;	
				return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
			}
			
			// Sticky Header
			$wp_customize->add_setting( 'sticky_header', array(
				'capability' => 'edit_theme_options',
				'sanitize_callback' => 'sanitize_select',
				'default' => 'no',
			) );
			
			$wp_customize->add_control( 'sticky_header', array(
				'type' => 'select',
				'section' => 'header_section', 
				'label' => __( 'Enable sticky header' ),
				'description' => __( '' ),
				'choices' => array(
					'yes' => __( 'Yes' ),
					'no' => __( 'No' ),
				),
			) );	 
			
			// Search
			$wp_customize->add_setting( 'search_popup', array(
				'capability' => 'edit_theme_options',
				'sanitize_callback' => 'sanitize_select',
				'default' => 'no',
			) );	
			
			$wp_customize->add_control( 'search_popup', array(
				'type' => 'select',
				'section' => 'header_section', 
				'label' => __( 'Enable search pop up icon' ),
				'description' => __( '' ),
				'choices' => array(
					'yes' => __( 'Yes' ),
					'no' => __( 'No' ),
				),
			) );
		}
	
	// Theme Customizer - Social Media
		/*add_action('customize_register', 'themecustomizer_socialmedia');
		add_action('customize_register', 'themecustomizer_footer');*/
	
		function themecustomizer_socialmedia( $wp_customize ) {
			$wp_customize->add_section( 'social_media', array(
				'title' => __( 'Social Media' ),
				'description' => __( 'Connect all your social media accounts.' ),
				'panel' => '', 
				'priority' => 30,
				'capability' => 'edit_theme_options',
				'theme_supports' => '',
			) );
			
			$wp_customize->add_setting('facebook_link');
			$wp_customize->add_setting('instagram_link');
			$wp_customize->add_setting('linkedin_link');
			$wp_customize->add_setting('twitter_link');			
			
			$wp_customize->add_control( 'facebook_link', array(
				'label' => __( 'Facebook' ),
				'type' => 'text',
				'section' => 'social_media',
			) );
			
			$wp_customize->add_control( 'linkedin_link', array(
				'label' => __( 'LinkedIn' ),
				'type' => 'text',
				'section' => 'social_media',
			) );
			
			$wp_customize->add_control( 'instagram_link', array(
				'label' => __( 'Instagram' ),
				'type' => 'text',
				'section' => 'social_media',
			) );
			
			$wp_customize->add_control( 'twitter_link', array(
				'label' => __( 'Twitter' ),
				'type' => 'text',
				'section' => 'social_media',
			) );
		}

		function themecustomizer_footer( $wp_customize ) {
			$wp_customize->add_section( 'footer', array(
				'title' => __( 'Footer' ),
				'description' => __( '' ),
				'panel' => '', 
				'priority' => 30,
				'capability' => 'edit_theme_options',
				'theme_supports' => '',
			) );
			
			$wp_customize->add_setting('footer_copyright');	
			
			$wp_customize->add_control( 'footer_copyright', array(
				'label' => __( 'Copyright' ),
				'type' => 'text',
				'section' => 'footer',
			) );	
		}
		
	// Extend Existing Gutenberg Blocks	
		// Block - Image
			/*register_block_style(
				'core/image',
				array(
					'name'         => 'fit-container',
					'label'        => 'Fit Container',
					'style_handle' => 'fitcontainer-style',
				)
			);*/
			
			register_block_style(
				'core/image',
				array(
					'name'         => 'rounded-border',
					'label'        => 'Rounded Border',
					'style_handle' => 'rounded-border-style',
				)
			);
			
			unregister_block_style( 'core/image', 'rounded-corners' );
			
		// Block - Spacer
			register_block_style( 
				'core/spacer',
				array(
					'name' => 'spacer25',
					'label' => 'Spacer 25px'
				)
			);
			
			register_block_style( 
				'core/spacer',
				array(
					'name' => 'spacer50',
					'label' => 'Spacer 50px'
				)
			);
			
			register_block_style( 
				'core/spacer',
				array(
					'name' => 'spacer100',
					'label' => 'Spacer 100px'
				)
			);

		// Block - Separator
			register_block_style( 
				'core/separator',
				array(
					'name' => 'left',
					'label' => 'Left'
				)
			);
			
			register_block_style( 
				'core/separator',
				array(
					'name' => 'right',
					'label' => 'Right'
				)
			);
			
	// Numbered Pagination
		if( !function_exists( 'numbered_pagination' ) )
		{
			function numbered_pagination() 
			{	
				// Vars
				global $query, $paged;				
				$countPage = $query->max_num_pages;
				
				// Options
				$commaEl = '';
				$tripledot = '<div class="nav-numbered static nav-tripledot">...</div>';
				$first_text = 
					'<svg width="100%" height="100%" viewBox="0 0 17 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;">
						<g transform="matrix(1,0,0,1,0,-80)">
							<g id="Left" transform="matrix(0.0489148,0,0,0.0550164,-2.87523,76.2183)">
								<rect x="58.78" y="68.738" width="347.543" height="308.999" style="fill:none;"/>
								<g transform="matrix(20.4437,0,0,18.1764,-74.1018,-1503.52)">
									<path d="M14.328,87.222L7.257,94.293C6.867,94.683 6.867,95.317 7.257,95.707L14.328,102.778C14.719,103.168 15.352,103.168 15.743,102.778C16.133,102.388 16.133,101.754 15.743,101.364L9.379,95C9.379,95 15.743,88.636 15.743,88.636C16.133,88.246 16.133,87.612 15.743,87.222C15.352,86.832 14.719,86.832 14.328,87.222ZM21.328,87.222L14.257,94.293C13.867,94.683 13.867,95.317 14.257,95.707L21.328,102.778C21.719,103.168 22.352,103.168 22.743,102.778C23.133,102.388 23.133,101.754 22.743,101.364L16.379,95C16.379,95 22.743,88.636 22.743,88.636C23.133,88.246 23.133,87.612 22.743,87.222C22.352,86.832 21.719,86.832 21.328,87.222Z"/>
								</g>
							</g>
						</g>
					</svg>';
					
				$prev_text = 
					'<svg width="100%" height="100%" viewBox="0 0 10 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;">
						<g transform="matrix(1,0,0,1,-2.02061e-14,-40)">
							<g id="Left" transform="matrix(0.0287734,0,0,0.0550164,-1.69131,36.2183)">
								<rect x="58.78" y="68.738" width="347.543" height="308.999" style="fill:none;"/>
								<g transform="matrix(34.7543,0,0,18.1764,-288.759,-776.465)">
									<path d="M19.243,61.364L12.879,55C12.879,55 19.243,48.636 19.243,48.636C19.633,48.246 19.633,47.612 19.243,47.222C18.852,46.832 18.219,46.832 17.828,47.222L10.757,54.293C10.367,54.683 10.367,55.317 10.757,55.707L17.828,62.778C18.219,63.168 18.852,63.168 19.243,62.778C19.633,62.388 19.633,61.754 19.243,61.364Z"/>
								</g>
							</g>
						</g>
					</svg>';
						
				$next_text = 
					'<svg width="100%" height="100%" viewBox="0 0 10 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;">
						<g transform="matrix(1,0,0,1,-40,-40)">
							<g id="Right" transform="matrix(0.0287734,0,0,0.0550164,38.3087,36.2183)">
								<rect x="58.78" y="68.738" width="347.543" height="308.999" style="fill:none;"/>
								<g transform="matrix(34.7543,0,0,18.1764,-1678.94,-776.465)">
									<path d="M52.172,62.778L59.243,55.707C59.633,55.317 59.633,54.683 59.243,54.293L52.172,47.222C51.781,46.832 51.148,46.832 50.757,47.222C50.367,47.612 50.367,48.246 50.757,48.636L57.121,55C57.121,55 50.757,61.364 50.757,61.364C50.367,61.754 50.367,62.388 50.757,62.778C51.148,63.168 51.781,63.168 52.172,62.778Z"/>
								</g>
							</g>
						</g>
					</svg>';
					
				$last_text = 
					'<svg width="100%" height="100%" viewBox="0 0 17 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;">
						<g transform="matrix(1,0,0,1,-40,-80)">
							<g id="Right" transform="matrix(0.0489148,0,0,0.0550164,37.1248,76.2183)">
								<rect x="58.78" y="68.738" width="347.543" height="308.999" style="fill:none;"/>
								<g transform="matrix(20.4437,0,0,18.1764,-891.855,-1503.52)">
									<path d="M54.257,88.636L60.621,95C60.621,95 54.257,101.364 54.257,101.364C53.867,101.754 53.867,102.388 54.257,102.778C54.648,103.168 55.281,103.168 55.672,102.778L62.743,95.707C63.133,95.317 63.133,94.683 62.743,94.293L55.672,87.222C55.281,86.832 54.648,86.832 54.257,87.222C53.867,87.612 53.867,88.246 54.257,88.636ZM47.257,88.636L53.621,95C53.621,95 47.257,101.364 47.257,101.364C46.867,101.754 46.867,102.388 47.257,102.778C47.648,103.168 48.281,103.168 48.672,102.778L55.743,95.707C56.133,95.317 56.133,94.683 55.743,94.293L48.672,87.222C48.281,86.832 47.648,86.832 47.257,87.222C46.867,87.612 46.867,88.246 47.257,88.636Z"/>
								</g>
							</g>
						</g>
					</svg>';
				
				// Check
				if ( empty( $paged ) == true )
				{
					$paged = 1;	
				}			
				if ( empty( $countPage ) == true )
				{
					$countPage = 1;	
				}

				// Show
				if ( $countPage > 1 )
				{
					// First / Prev
					if ( $paged > 2 )
					{
						echo '<a href="' . get_pagenum_link( 1 ) . '"><div class="nav-numbered nav-arrow nav-first">' . $first_text . '</div></a>';
						echo '<a href="' . get_pagenum_link( $paged - 1 ) . '"><div class="nav-numbered nav-arrow nav-prev">' . $prev_text . '</div></a>';						
					}
					else if ( $paged > 1 )
					{
						echo '<a href="' . get_pagenum_link( $paged - 1 ) . '"><div class="nav-numbered nav-arrow nav-first">' . $first_text . '</div></a>';					
					}
					
					// Numbers
					$max_page_shown = 5; // Need to be odd number
					$adjacent_page_shown = ( $max_page_shown - ( $max_page_shown % 2 ) ) / 2;				
					
					if ( $paged < $adjacent_page_shown )
					{
						if ( $countPage < $max_page_shown )
						{
							for( $i = 1; $i <=  $countPage; $i++ )
							{
								if ( $i == $countPage )
								{
									$commaEl = '';	
								}
								
								if ( $i == $paged )
								{
									echo '<div class="nav-numbered static">' . $i . $commaEl . '</div>';
								}
								else 
								{ 
									echo '<a href="' . get_pagenum_link( $i ) . '"><div class="nav-numbered">' . $i . $commaEl . '</div></a>';
								}
							} 
						}
						else
						{
							for( $i = 1; $i <= $max_page_shown; $i++ )
							{
								if ( $i == $max_page_shown)
								{
									$commaEl = '';	
								}
								
								if ( $i == $paged )
								{
									echo '<div class="nav-numbered static">' . $i . $commaEl . '</div>';
								}
								else 
								{ 
									echo '<a href="' . get_pagenum_link( $i ) . '"><div class="nav-numbered">' . $i . $commaEl . '</div></a>';
								}
							}												
						}
					}
					elseif ($paged > $countPage - $adjacent_page_shown)
					{
						if ( $countPage < $max_page_shown )
						{
							for($i = 1; $i <=  $countPage; $i++)
							{
								if ( $i == $countPage)
								{
									$commaEl = '';	
								}
								
								if ($i == $paged)
								{
									echo '<div class="nav-numbered static">' . $i . $commaEl .'</div>';
								}
								else 
								{ 
									echo '<a href="' . get_pagenum_link( $i ) . '"><div class="nav-numbered">' . $i .  $commaEl . '</div></a>'; 
								}
							} 
						}
						else
						{
							for($i = ( $countPage - ( $max_page_shown -1 ) ); $i <=  $countPage; $i++)
							{
								if ( $i == $countPage)
								{
									$commaEl = '';	
								}
								
								if ($i == $paged)
								{
									echo '<div class="nav-numbered static">' . $i .  $commaEl .'</div>';
								}
								else 
								{ 
									echo '<a href="' . get_pagenum_link( $i ) . '"><div class="nav-numbered">' . $i .  $commaEl .'</div></a>';
								}
							}
						}
					}
					else
					{  
						if ( ( $paged + $adjacent_page_shown ) < $countPage )
						{
							$maxRest = max( $paged + $adjacent_page_shown, $max_page_shown );
						}
						else 
						{
							$maxRest = min( $paged + $adjacent_page_shown, $countPage );
						}
					
						for($i = max(1, $paged - $adjacent_page_shown ); $i <= $maxRest; $i++)
						{
							if ( $i == $maxRest )
							{							
								$commaEl = '';	
							}
							
							if ( $i == $paged )
							{
								echo '<div class="nav-numbered static">' . $i .  $commaEl .'</div>';
							}
							else 
							{ 
								echo '<a href="' . get_pagenum_link( $i ) . '"><div class="nav-numbered">' . $i . $commaEl . '</div></a>';
							}  
						} 
					}
					
					// Triple Dot - Simply Before Next / Last
					/*
					if ( ( $paged + $adjacent_page_shown ) < $countPage && $countPage > $max_page_shown )
					{					
						echo '<div class="nav-numbered static nav-tripledot">...</div>';
					}
					*/
					
					// Triple Dot - Before Next / Last WITH Last Page Nav Button Added
					if ( ( $paged + $adjacent_page_shown ) < $countPage - 1 && $countPage > $max_page_shown ) 
					{
						echo '<div class="nav-numbered static nav-tripledot">...</div>';
						echo '<a href="' . get_pagenum_link( $countPage ) . '"><div class="nav-numbered">' . $countPage . $commaEl . '</div></a>';
					}				
					
					// Next / Last
					if ( $paged == $countPage - 1 && $countPage > $max_page_shown )
					{
						echo '<a href="' . get_pagenum_link( $countPage ) . '"><div class="nav-numbered nav-arrow nav-last">' . $last_text . '</div></a>';	
					}				
					else if ($paged < $countPage && $countPage > $max_page_shown  )
					{					
						echo '<a href="' . get_pagenum_link( $paged + 1 ) . '"><div class="nav-numbered nav-arrow  nav-next">' . $next_text . '</div></a>';
						echo '<a href="' . get_pagenum_link( $countPage ) . '"><div class="nav-numbered nav-arrow nav-last">' . $last_text . '</div></a>';
					}				
				}
			}
		}
	
	// Metabox		
		// PAGE Metabox
		add_action( 'add_meta_boxes', 'add_page_metabox' );
		add_action( 'save_post', 'page_metabox_save' );	
		
		function add_page_metabox() 
		{		
			add_meta_box(
				'page_metabox', 
				'Page Options',
				'page_metabox_html', 
				'page'
			);		
		}	
		
		function page_metabox_html( $post ) 
		{
			$show_page_title = get_post_meta( $post->ID, '_show_page_title_meta_key', true );
			$transparent_menu = get_post_meta( $post->ID, '_transparent_menu_meta_key', true );
			$choose_sidebar = get_post_meta( $post->ID, '_choose_sidebar_meta_key', true );
			
			if ( empty( $transparent_menu ) ) {
				$transparent_menu = 'no';	
			}

			?>
				<div class="metabox-row">
                    <label for="choose_sidebar_field">Sidebar</label>
                    <select name="choose_sidebar_field" id="choose_sidebar_field" class="postbox">
						<?php 
			
							foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) 
							{ 
								?>
							 		<option value="<?php echo $sidebar['id']; ?>" <?php selected( $choose_sidebar, $sidebar['id'] ); ?>>
										<?php echo ucwords( $sidebar['name'] ); ?>
									</option>
								<?php 
							} 
						?>                        
                    </select>
                </div>

            	<div class="metabox-row">
                    <label for="show_page_title_field">Show Page Title</label>
                    <select name="show_page_title_field" id="show_page_title_field" class="postbox">
                        <option value="yes" <?php selected( $show_page_title, 'yes' ); ?>>Yes</option>
                        <option value="no" <?php selected( $show_page_title, 'no' ); ?>>No</option>
                    </select>
                </div>
                
                <div class="metabox-row">
                    <label for="transparent_menu_field">Alternate Header</label>
                    <select name="transparent_menu_field" id="transparent_menu_field" class="postbox">
                        <option value="yes" <?php selected( $transparent_menu, 'yes' ); ?>>Yes</option>
                        <option value="no" <?php selected( $transparent_menu, 'no' ); ?>>No</option>
                    </select>
                    <div class="metabox-description">
	                    Enabling this will transform header in this page into transparent header without background color and overlayed in front of content. Best paired with image/cover as the first block.<br><br>
                        Alternate logo will be used if you upload it in theme customizer (Appearance - Customize - Site identity).<br><br>
                        
                        Does not make header sticky. You can turn on sticky header option in theme customizer (Appearance - Customize - Header).
                    </div>
                </div>
			<?php
		}		
		
		function page_metabox_save( $post_id ) 
		{
			if ( array_key_exists( 'choose_sidebar_field', $_POST ) ) {
				update_post_meta(
					$post_id,
					'_choose_sidebar_meta_key',
					$_POST['choose_sidebar_field']
				);
			}
			
			if ( array_key_exists( 'show_page_title_field', $_POST ) ) {
				update_post_meta(
					$post_id,
					'_show_page_title_meta_key',
					$_POST['show_page_title_field']
				);
			}
			
			if ( array_key_exists( 'transparent_menu_field', $_POST ) ) {
				update_post_meta(
					$post_id,
					'_transparent_menu_meta_key',
					$_POST['transparent_menu_field']
				);
			}
		}
		
	// Woocommerce			
		// Retake back sidebar
		    add_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
			
		// Archive - Column 
			add_filter('loop_shop_columns', 'loop_columns', 999);
			
			if (!function_exists('loop_columns')) 
			{
				function loop_columns() 
				{
					return 4;
				}
			}
			
		// Get Vendor Name
			function get_vendor_name() {
				global $post;
				
				$sold_by = get_option( 'wcpv_vendor_settings_display_show_by', 'yes' );
				
				if ( 'yes' === $sold_by ) 
				{			
					$sold_by = WC_Product_Vendors_Utils::get_sold_by_link( $post->ID );
					
					echo '<div class="woocommerce-vendor-wrapper"><a href="' . esc_url( $sold_by['link'] ) . '" title="' . esc_attr( $sold_by['name'] ) . '">' . $sold_by['name'] . '</a></div>';
				}
				
				return true;
			}
		
		// Search - Enable on Woocommerce
			/*add_filter('pre_get_posts','woocommerce_search_filter', 9);
			
			function woocommerce_search_filter($query) 
			{			
				if( isset($_GET['search-type']) && $_GET['search-type']){
					$type = $_GET['search-type'];
					
					if ( $query->is_search && !is_admin() && $type == 'shop' ) 
					{
						$query->set( 'post_type', 'product' );
						$query->is_post_type_archive = true;
					}
				}
			}*/

/* WIDGET */
	/* Separator */
		function register_widget_separator() {
			register_widget( 'seedlet_separator' );
		}
		
		add_action( 'widgets_init', 'register_widget_separator' );
	
		class seedlet_separator extends WP_Widget 
		{
			function __construct() 
			{
				parent::__construct(
					// widget ID
					'seedlet_separator',
					// widget name
					__('Separator', ' seedlet'),
					// widget description
					array( 'description' => __( 'Line between widgets', 'seedlet' ), )
				);
			}
			
			public function widget( $args, $instance ) {
				/*$title = apply_filters( 'widget_title', $instance['title'] );*/
				echo $args['before_widget'];
				/*//if title is present
				if ( ! empty( $title ) )
				echo $args['before_title'] . $title . $args['after_title'];
				//output
				echo __( 'Separator', 'seedlet' );*/
					
				?>
					<div class="widget-separator">
					</div>
				<?php
					
				echo $args['after_widget'];			
			}
			
			public function form( $instance ) {
				/*if ( isset( $instance[ 'title' ] ) )
					$title = $instance[ 'title' ];
				else
					$title = __( 'Default Title', 'seedlet' );
				?>
					<p>
						<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
						<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
					</p>
				<?php*/
			}
			
			public function update( $new_instance, $old_instance ) {
				$instance = array();
				//$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
				return $instance;
			}
		}

	/* Button */
		function register_widget_std_button() {
			register_widget( 'std_button' );
		}
		
		add_action( 'widgets_init', 'register_widget_std_button' );
	
		class std_button extends WP_Widget 
		{
			function __construct() 
			{
				parent::__construct(
					// widget ID
					'std_button',
					// widget name
					__('Button', ' seedlet'),
					// widget description
					array( 'description' => __( 'Create button', 'seedlet' ), )
				);
			}
			
			public function widget( $args, $instance ) {			
				$url = apply_filters( 'widget_title', $instance['url'] );
				$text = apply_filters( 'widget_title', $instance['text'] );
				$style = apply_filters( 'widget_title', $instance['style'] );
				
				/*//if title is present
				if ( ! empty( $title ) )
				echo $args['before_title'] . $title . $args['after_title'];
				//output
				echo __( 'Separator', 'seedlet' );*/
				
				
				if ( !empty( $text ) )
				{
					$classEl = '';
					if ( $style == 'outline' ) {
						$classEl = 'alt';
					}
					
					echo $args['before_widget'];
					?>
						<a href="<?php echo $url; ?>" title="<?php echo $text; ?>">
							<div class="std-button <?php echo $classEl; ?>">
								<?php echo $text; ?>
							</div>
						</a>
					<?php
					echo $args['after_widget'];	
				}			
						
			}
			
			public function form( $instance ) {
				
				if ( isset( $instance[ 'url' ] ) ) {
					$url = $instance[ 'url' ];
				}
				else {
					$url = __( '#', 'seedlet' );
				}
				
				if ( isset( $instance[ 'text' ] ) ) {
					$text = $instance[ 'text' ];
				}
				else {
					$text = __( 'Button Text', 'seedlet' );
				}
				
				if ( isset( $instance[ 'style' ] ) ) {
					$style = $instance[ 'style' ];
				}
				else {
					$style = __( 'fill', 'seedlet' );
				}
				
				?>
					<p>
						<label for="<?php echo $this->get_field_id( 'url' ); ?>"><?php _e( 'Button URL:' ); ?></label>
						<input class="widefat" id="<?php echo $this->get_field_id( 'url' ); ?>" name="<?php echo $this->get_field_name( 'url' ); ?>" type="text" value="<?php echo esc_attr( $url ); ?>" />
					</p>
					<p>
						<label for="<?php echo $this->get_field_id( 'text' ); ?>"><?php _e( 'Button Text:' ); ?></label>
						<input class="widefat" id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name( 'text' ); ?>" type="text" value="<?php echo esc_attr( $text ); ?>" />
					</p>
					<p>
						<label for="<?php echo $this->get_field_id( 'style' ); ?>"><?php _e( 'Button Style:' ); ?></label>					
						<select class="widefat" id="<?php echo $this->get_field_id( 'style' ); ?>" name="<?php echo $this->get_field_name( 'style' ); ?>">
							<option value="fill" <?php selected( $style, "fill" ); ?>>Fill</option>
							<option value="outline" <?php selected( $style, "outline" ); ?>>Outline</option>
						</select>
					</p>
				<?php	
			}
			
			public function update( $new_instance, $old_instance ) {
				$instance = array();
				$instance['url'] = ( ! empty( $new_instance['url'] ) ) ? strip_tags( $new_instance['url'] ) : '';
				$instance['text'] = ( ! empty( $new_instance['text'] ) ) ? strip_tags( $new_instance['text'] ) : '';
				$instance['style'] = ( ! empty( $new_instance['style'] ) ) ? strip_tags( $new_instance['style'] ) : 'fill';
				return $instance;
			}
		}
	
		require get_stylesheet_directory() . "/inc/classes/class-membership.php";	
		
	/* Latest Event */
		function register_widget_latest_event() {
			register_widget( 'latest_event' );
		}
		
		//add_action( 'widgets_init', 'register_widget_latest_event' );
	
		class latest_event extends WP_Widget 
		{
			function __construct() 
			{
				parent::__construct(
					// widget ID
					'latest_event',
					// widget name
					__('Latest Event', ' seedlet'),
					// widget description
					array( 'description' => __( 'Show the latest  event.', 'seedlet' ), )
				);
			}
			
			public function widget( $args, $instance ) {
				/*$title = apply_filters( 'widget_title', $instance['title'] );*/
				echo $args['before_widget'];
				/*//if title is present
				if ( ! empty( $title ) )
				echo $args['before_title'] . $title . $args['after_title'];
				//output
				echo __( 'Separator', 'seedlet' );*/
				
				$args = array(
					'post_type' => array( 'event' ),
				);
				
				$query = new WP_Query( $args );
				
				if ( $query->have_posts() ) 
				{
					while ( $query->have_posts() ) 
					{
						$query->the_post();
						
						echo $args['before_title'] . get_the_title() . $args['after_title'];						
						?>
                        	<div class="last-event-wrapper">
                            	<?php the_content(); ?>
                            </div>
                        <?php
					}
				} 
				
				wp_reset_postdata();
				
				echo $args['after_widget'];			
			}
			
			public function form( $instance ) {
				/*if ( isset( $instance[ 'title' ] ) )
					$title = $instance[ 'title' ];
				else
					$title = __( 'Default Title', 'seedlet' );
				?>
					<p>
						<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
						<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
					</p>
				<?php*/
			}
			
			public function update( $new_instance, $old_instance ) {
				$instance = array();
				//$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
				return $instance;
			}
		}
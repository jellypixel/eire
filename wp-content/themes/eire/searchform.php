  	<?php
	  	$homeurl = home_url( '/' );
	?>
    <form role="search" id="searchform" method="get" action="<?php echo esc_url($homeurl); ?>">
        <label class="screen-reader-text" for="s"><?php esc_html_e( 'Search ', 'seedlet' ); ?></label>		
        <input type="text" id="s" name="s" placeholder="<?php esc_html_e( 'type here to search', 'seedlet' ); ?>" value="<?php the_search_query(); ?>" required>		
        <input type="submit" id="searchsubmit" value="<?php esc_html_e( 'Search', 'seedlet' ); ?>">
    </form>
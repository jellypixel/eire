<?php
	get_header();
	
	$mainColumn = 'col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 slim';
	if ( is_active_sidebar( 'single-sidebar' ) ) 
	{
		$mainColumn = 'col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12';
	}
?>

	<?php
        if ( have_posts() )
        {
            while( have_posts() )
            {
                the_post();	
                
                global $post;
                
                $postThumb = get_the_post_thumbnail_url( $post, 'full' );	
                
                ?>
                <div id="content" class="site-content single-wrapper">
                    <div class="entry-content">
                    
                    	<div class="single-title">
                            <h1><?php the_title(); ?></h1>
                        </div>
                        
                        <div class="single-meta-wrapper">
                        	<div class="blog-meta-date">
								<svg viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;">
                                    <g transform="matrix(1,0,0,1,-160,-80)">
                                        <g id="Time" transform="matrix(0.0629083,0,0,0.0616947,113.247,60.6881)">
                                            <rect x="743.187" y="313.024" width="413.3" height="421.43" style="fill:none;"/>
                                            <clipPath id="_clip1">
                                                <rect x="743.187" y="313.024" width="413.3" height="421.43"/>
                                            </clipPath>
                                            <g clip-path="url(#_clip1)">
                                                <g transform="matrix(15.8961,0,0,16.2089,-1800.2,-983.685)">
                                                    <path d="M173,80C165.825,80 160,85.825 160,93C160,100.175 165.825,106 173,106C180.175,106 186,100.175 186,93C186,85.825 180.175,80 173,80ZM172,84.964L172,93C172,93.552 172.448,94 173,94L179,94C179.552,94 180,93.552 180,93C180,92.448 179.552,92 179,92L174,92C174,92 174,84.964 174,84.964C174,84.412 173.552,83.964 173,83.964C172.448,83.964 172,84.412 172,84.964Z"/>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>

                                <?php
                                    $dateLink = get_month_link( get_the_time('Y'), get_the_time('n') );																
                                ?>
                                <a href="<?php echo esc_url( $dateLink ); ?>" title="<?php the_date(); ?>" rel="bookmark">
                                    <time class="entry-date" datetime="<?php echo esc_attr(get_the_date('Y-m-j')); ?>">
                                        <?php echo esc_html( get_the_date( 'M j, Y' ) ); ?>
                                    </time>
                                </a>	
                            </div>
                            <div class="blog-meta-top">                                        	
                                <div class="blog-meta-author">
                                    <svg viewBox="0 0 25 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;">
                                        <g transform="matrix(1,0,0,1,-120,-80)">
                                            <g id="Author" transform="matrix(0.0604888,0,0,0.0616947,75.0455,60.6881)">
                                                <rect x="743.187" y="313.024" width="413.3" height="421.43" style="fill:none;"/>
                                                <clipPath id="_clip1">
                                                    <rect x="743.187" y="313.024" width="413.3" height="421.43"/>
                                                </clipPath>
                                                <g clip-path="url(#_clip1)">
                                                    <g transform="matrix(16.532,0,0,16.2089,-1240.65,-983.685)">
                                                        <path d="M124.023,105.997L141.024,105.961C142.359,105.958 143.604,105.29 144.34,104.182C145.076,103.073 145.205,101.671 144.683,100.449C142.776,95.846 138.051,92.571 132.519,92.571C126.988,92.571 122.263,95.845 120.321,100.431C119.793,101.662 119.922,103.081 120.666,104.202C121.411,105.323 122.672,105.998 124.023,105.997ZM132.5,80.003C129.431,80.003 126.94,82.494 126.94,85.563C126.94,88.632 129.431,91.123 132.5,91.123C135.569,91.123 138.06,88.632 138.06,85.563C138.06,82.494 135.569,80.003 132.5,80.003Z"/>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>

                                    <?php 
                                        the_author_posts_link(); 
                                    ?>   
                                </div>
                                <div class="blog-meta-category">
                                    <svg viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;">
                                        <g transform="matrix(1,0,0,1,-40,-80)">
                                            <g id="Category" transform="matrix(0.0629083,0,0,0.0616947,-6.75264,60.6881)">
                                                <rect x="743.187" y="313.024" width="413.3" height="421.43" style="fill:none;"/>
                                                <clipPath id="_clip1">
                                                    <rect x="743.187" y="313.024" width="413.3" height="421.43"/>
                                                </clipPath>
                                                <g clip-path="url(#_clip1)">
                                                    <g transform="matrix(15.8961,0,0,16.2089,107.341,-983.685)">
                                                        <path d="M52,97C52,96.204 51.684,95.441 51.121,94.879C50.559,94.316 49.796,94 49,94C47.257,94 44.743,94 43,94C42.204,94 41.441,94.316 40.879,94.879C40.316,95.441 40,96.204 40,97C40,98.743 40,101.257 40,103C40,103.796 40.316,104.559 40.879,105.121C41.441,105.684 42.204,106 43,106C44.743,106 47.257,106 49,106C49.796,106 50.559,105.684 51.121,105.121C51.684,104.559 52,103.796 52,103L52,97ZM66,97C66,96.204 65.684,95.441 65.121,94.879C64.559,94.316 63.796,94 63,94C61.257,94 58.743,94 57,94C56.204,94 55.441,94.316 54.879,94.879C54.316,95.441 54,96.204 54,97C54,98.743 54,101.257 54,103C54,103.796 54.316,104.559 54.879,105.121C55.441,105.684 56.204,106 57,106C58.743,106 61.257,106 63,106C63.796,106 64.559,105.684 65.121,105.121C65.684,104.559 66,103.796 66,103L66,97ZM52,83C52,82.204 51.684,81.441 51.121,80.879C50.559,80.316 49.796,80 49,80C47.257,80 44.743,80 43,80C42.204,80 41.441,80.316 40.879,80.879C40.316,81.441 40,82.204 40,83C40,84.743 40,87.257 40,89C40,89.796 40.316,90.559 40.879,91.121C41.441,91.684 42.204,92 43,92C44.743,92 47.257,92 49,92C49.796,92 50.559,91.684 51.121,91.121C51.684,90.559 52,89.796 52,89L52,83ZM66,83C66,82.204 65.684,81.441 65.121,80.879C64.559,80.316 63.796,80 63,80C61.257,80 58.743,80 57,80C56.204,80 55.441,80.316 54.879,80.879C54.316,81.441 54,82.204 54,83C54,84.743 54,87.257 54,89C54,89.796 54.316,90.559 54.879,91.121C55.441,91.684 56.204,92 57,92C58.743,92 61.257,92 63,92C63.796,92 64.559,91.684 65.121,91.121C65.684,90.559 66,89.796 66,89L66,83Z"/>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>

                                    <?php the_category(', '); ?>
                                </div>
                                <div class="blog-comments">
                                    <svg width="100%" height="100%" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;">
                                        <g transform="matrix(1,0,0,1,-200,-80)">
                                            <g id="Comment" transform="matrix(0.0629083,0,0,0.0616947,153.247,60.6881)">
                                                <rect x="743.187" y="313.024" width="413.3" height="421.43" style="fill:none;"/>
                                                <clipPath id="_clip1">
                                                    <rect x="743.187" y="313.024" width="413.3" height="421.43"/>
                                                </clipPath>
                                                <g clip-path="url(#_clip1)">
                                                    <g transform="matrix(15.8961,0,0,16.2089,-2436.04,-983.685)">
                                                        <path d="M212.586,100L218.293,105.707C218.579,105.993 219.009,106.079 219.383,105.924C219.756,105.769 220,105.404 220,105L220,100C220,100 222,100 222,100C224.209,100 226,98.209 226,96L226,84C226,81.791 224.209,80 222,80C217.389,80 208.611,80 204,80C201.791,80 200,81.791 200,84L200,96C200,98.209 201.791,100 204,100L212.586,100ZM207,94L219,94C219.552,94 220,93.552 220,93C220,92.448 219.552,92 219,92L207,92C206.448,92 206,92.448 206,93C206,93.552 206.448,94 207,94ZM207,88L219,88C219.552,88 220,87.552 220,87C220,86.448 219.552,86 219,86L207,86C206.448,86 206,86.448 206,87C206,87.552 206.448,88 207,88Z"/>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                
                                    <?php
                                        if ( comments_open() ) 
                                        {																
                                            comments_popup_link( esc_html__( '0', 'seedlet' ), esc_html__( '1', 'seedlet' ), esc_html__( '%', 'seedlet' ) ); 													
                                        }
                                    ?>
                                </div>
                                <div class="blog-meta-edit">                                            	
                                    <?php
                                        if ( current_user_can( 'edit_posts' ) )
                                        {
                                            ?>
                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     viewBox="123.3 0 595.3 595.3" enable-background="new 123.3 0 595.3 595.3" xml:space="preserve">
                                                    <g>
                                                        <path d="M453,595.4h-61.6c-11.7,0-15.1,0-47.1-80.8l-21.3-8.9c-65.7,30.5-73.6,30.5-77,30.5c-4.3,0-9.2-1.9-12.3-5.1l-43.5-43.6
                                                            c-8.7-8.7-10.9-11,23.6-91l-8.6-20.7C124,346.1,124,342.5,124,330.1v-61.6c0-12.3,0-15.5,81-47.5l8.6-20.8
                                                            c-36.5-78.5-33.9-81-25.2-89.8l43.6-43.6c3.1-3.1,7.9-5,12.3-5c3.2,0,11.4,0,78.2,28.8l21.2-8.8c29.7-81.2,33.5-81.2,45.3-81.2
                                                            h61.6c11.7,0,15.1,0,47.1,80.9l21.3,8.8c65.7-30.4,73.6-30.4,77-30.4c4.3,0,9.2,1.9,12.2,5l43.7,43.5c8.5,8.5,10.9,11-23.6,91
                                                            l8.6,20.9c81.3,29.6,81.3,33.2,81.3,45.6v61.6c0,12.4,0,15.5-81,47.6l-8.5,20.8c36.5,78.4,33.9,81,25.3,89.8l-43.7,43.6
                                                            c-3.1,3.1-7.9,5-12.3,5l0,0c-3.1,0-11.3,0-78.1-28.9l-21.3,8.8C468.5,595.4,465.1,595.4,453,595.4z M400.3,561.8h43.6
                                                            c5.8-12.7,17-41.4,26-66.2c1.6-4.4,5-8,9.2-9.7l34.3-14.2c4-1.7,8.6-1.6,12.6,0.2c24.7,10.7,53.9,22.7,67.1,27.2l30.5-30.6
                                                            c-4.9-13.2-17.3-41.2-28.3-64.8c-2-4.3-2.1-9.3-0.3-13.8l14.1-34.4c1.7-4.2,5.1-7.5,9.2-9.1c24.9-9.8,53.8-22,66.5-28.1v-42.8
                                                            c-12.9-5.8-41.6-17-66.3-25.9c-4.3-1.6-7.9-5-9.7-9.3L595,205.8c-1.7-4.3-1.7-9.1,0.1-13.4c10.6-24.4,22.4-53.2,27-66.6l-30.6-30.5
                                                            c-13.1,4.8-41.5,17.3-65.5,28.5c-4,1.9-8.8,2.1-13,0.3L478.7,110c-4.1-1.7-7.5-5.1-9.1-9.3c-9.8-24.9-21.8-53.8-28-66.5h-43.6
                                                            c-5.8,12.7-17,41.5-26,66.2c-1.6,4.5-5,8-9.2,9.8l-34.2,14.2c-4.1,1.7-8.6,1.6-12.6-0.1c-24.7-10.7-54-22.7-67.1-27.2l-30.6,30.5
                                                            c4.9,13.2,17.3,41.3,28.3,64.9c2,4.3,2.1,9.3,0.3,13.8l-14.2,34.3c-1.7,4.2-5,7.5-9.2,9.1c-24.9,9.8-53.8,21.9-66.4,28v42.9
                                                            c12.8,5.9,41.5,17,66.2,26c4.3,1.6,7.9,5,9.6,9.3l14.1,34.3c1.8,4.2,1.7,9.1-0.1,13.3c-10.6,24.4-22.4,53.2-27,66.6l30.6,30.5
                                                            c13.1-4.8,41.5-17.4,65.5-28.5c4.1-1.9,8.8-2,13-0.3l34.3,14.2c4.2,1.7,7.4,5.1,9.1,9.3C382,520.3,394.1,549.2,400.3,561.8z
                                                             M420.9,404.8c-58.7,0-106.4-47.9-106.4-106.8c0-58.8,47.8-106.7,106.4-106.7c58.6,0,106.4,47.9,106.4,106.7
                                                            C527.3,356.8,479.6,404.8,420.9,404.8z M420.9,225c-40.5,0-73.5,32.8-73.5,73.1c0,40.3,33,73.1,73.5,73.1
                                                            c40.5,0,73.5-32.8,73.5-73.1C494.4,257.7,461.5,225,420.9,225z"/>
                                                    </g>
                                                </svg>
                                            <?php														
                                            edit_post_link( esc_html__( 'Edit', 'seedlet' ), '<span class="edit-link">', '</span>' );																		
                                        }
                                    ?>
                                </div>
                                                                            
                            </div>
                        </div>
                    
                        <?php
                            if ( !empty( $postThumb ) )
                            {
                                ?>
                                    <div class="single-thumb alignwide">
                                        <img src="<?php echo $postThumb; ?>" alt="<?php the_title(); ?>">                                        
                                    </div>
                                <?php
                            }
                        ?>
                    
                        <div class="container">
                            <div class="row">                            
                                <div class="<?php echo $mainColumn; ?>">                                    
                                    <div class="single-content">                            
                                        <div class="single-description">
                                            <?php the_content(); ?>
                                        </div>
                                        
                                        <div class="single-social-wrapper">
                                            <div class="single-social-title">
                                                Share
                                            </div>
                                            <div class="single-social">
                                                <?php get_template_part( 'section', 'sharer' ); ?>
                                            </div>                                    
                                        </div>
                                                                        
                                        <?php 		
                                            // Prev / Next Post
                                            $next_post = get_adjacent_post(false, '', false);
                                            $prev_post = get_adjacent_post(false, '', true);
                                            
                                            if( !empty( $next_post ) || !empty( $prev_post ) ) 
                                            {
                                                ?>
                                                    <div class="single-nav-wrapper">    
                                                        <?php                                                	
                                                            if( !empty( $next_post ) ) 
                                                            {
                                                                $nextpostThumb = get_the_post_thumbnail_url( $next_post, 'medium' );
                                                                ?>
                                                                    <div class="single-nav-block next-post">
                                                                        <a href="<?php the_permalink( $next_post->ID ); ?>" title="<?php echo $next_post->post_title; ?>">
                                                                            <div class="single-nav">                                                            	                                                                    <div class="single-nav-thumb" style="background-image:url(<?php echo $nextpostThumb; ?>);">
                                                                                </div>
                                                                                <div class="single-nav-title-wrapper">
                                                                                    <div class="single-nav-title-label">
                                                                                        Previous
                                                                                    </div>
                                                                                    <div class="single-nav-title">
                                                                                        <?php echo $next_post->post_title; ?>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                <?php
                                                            }												
                                                            
                                                            if( !empty( $prev_post ) ) 
                                                            {
                                                                $prevpostThumb = get_the_post_thumbnail_url( $prev_post, 'medium' );
                                                                ?>
                                                                    <div class="single-nav-block prev-post">
                                                                        <a href="<?php the_permalink($prev_post->ID); ?>" title="<?php echo $prev_post->post_title; ?>">
                                                                            <div class="single-nav">                          
                                                                                <div class="single-nav-title-wrapper">
                                                                                    <div class="single-nav-title-label">
                                                                                        Next
                                                                                    </div>
                                                                                    <div class="single-nav-title">
                                                                                        <?php echo $prev_post->post_title; ?>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="single-nav-thumb" style="background-image:url(<?php echo $prevpostThumb; ?>);">
                                                                                </div>                                                                    
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                <?php
                                                            }
                                                        ?>
                                                    </div>
                                                <?php
                                            }
            
                                            // Author
                                            get_template_part( 'section', 'author' ); 
											
											// Related
                                            get_template_part( 'section', 'related' ); 
                                        ?>
                                        <div class="single-comments-wrapper">                                        	
                                            <?php
                                                // Comments
                                                comments_template( '', true );
                                            ?>
                                        </div>                                                        
                                    </div>
                                                
                                </div>
                                
                                <?php
                                    if ( is_active_sidebar( 'single-sidebar' ) ) 
                                    {
                                        ?>
                                            <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                                                <div class="single-sidebar">
                                                    <?php dynamic_sidebar( 'single-sidebar' ); ?>       
                                                </div>
                                            </div>
                                        <?php							
                                    }
                                ?>
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            <?php
			}
		}
	?>
<?php
	get_footer();
?>


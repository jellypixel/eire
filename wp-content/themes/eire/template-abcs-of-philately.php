<?php
/*
	Template Name: Archive: ABCs of Philately
	
	Not actual archive page, just bridge to get custom post content
	use archive-<cpt name>.php for actual archive
*/
	wp_redirect( get_post_type_archive_link( 'abcs_of_philately' ) );
	exit;
?>
